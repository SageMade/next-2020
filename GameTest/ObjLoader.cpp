#include "stdafx.h"
#include "ObjLoader.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <set>
#include <functional>

// Borrowed from https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
#pragma region String Trimming

// trim from start (in place)
static inline void ltrim(std::string & s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
static inline void rtrim(std::string & s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string & s) {
	ltrim(s);
	rtrim(s);
}

#pragma endregion

template <typename ResultT, ResultT OffsetBasis, ResultT Prime, typename =std::enable_if_t<std::is_unsigned_v<ResultT>>>
class FNVA_Hash final {
public:
	using result_type = ResultT;

private:
	result_type _state = OffsetBasis;

public:
	constexpr ResultT Hash(const void* data, std::size_t size) noexcept {
		const unsigned char* cdata = static_cast<const unsigned char*>(data);
		ResultT acc = this->_state;
		for (size_t i = 0; i < size; ++i) {
			ResultT next = cdata[i];
			acc = (acc ^ next) * Prime;
		}
		this->_state = acc;
		return this->_state;
	}
	template <typename T>
	constexpr ResultT Hash(const T& data) noexcept {
		const unsigned char* cdata = reinterpret_cast<const unsigned char*>(&data);
		ResultT acc = _state;
		for (size_t i = 0; i < sizeof(T); ++i) {
			ResultT next = cdata[i];
			acc = (acc ^ next) * Prime;
		}
		_state = acc;
		return _state;
	}
};

using FNVA32 = FNVA_Hash<std::uint32_t, 2166136261, 16777619>;
using FNVA64 = FNVA_Hash<std::uint64_t, 14695981039346656037, 1099511628211>;

template <std::size_t Bits>
struct FNVA1A_t;

template <>
struct FNVA1A_t<32> { using type = FNVA32; };
template <>
struct FNVA1A_t<64> {	using type = FNVA64; };

using FNVA1A = typename FNVA1A_t<CHAR_BIT * sizeof(size_t)>::type;


struct Vertex {
	vec3 Position;
	vec3 Color;

	size_t GetHash() const {
		FNVA1A hasher;
		return hasher.Hash(this);
	}
};
inline bool operator ==(const Vertex& l, const Vertex& r) {
	return (l.Position == r.Position) & (l.Color == r.Color);
}
inline bool operator !=(const Vertex& l, const Vertex& r) {
	return !(l == r);
}
bool operator <(const Vertex& r, const Vertex& l) {
	return l.GetHash() < r.GetHash();
}

LineMesh::Sptr ObjLoader::LoadMesh(const std::string& filename) {
	// Open our file in binary mode
	std::ifstream file;
	file.open(filename, std::ios::binary);

	// If our file fails to open, we will throw an error
	if (!file) {
		throw std::runtime_error("Failed to open file");
	}

	// Declare vectors for our positions, normals, and face indices
	std::vector<vec3>   positions;
	// We use a set for our lines, since we want all lines to be duplicated, and sorted insertion is O(logn) for sets
	// We are also using a single 'key' for each line, with the lower and upper bits indicating the source vertices
	std::set<uint64_t>  lines;
	// Stores our w value if it is provided
	float garb;
	// Stores the line that we are operating on
	std::string line;
	
	// Iterate as long as there is content to read
	while (std::getline(file, line)) {
		// v is our position
		if (line.substr(0, 2) == "v ") {
			// Read in the position and append it to our positions list
			std::istringstream ss = std::istringstream(line.substr(2));
			vec3 pos(0.0f); ss >> pos.X; ss >> pos.Y; ss >> pos.Z; ss >> garb;
			positions.push_back(pos);
		}
		// f is our faces
		else if (line.substr(0, 2) == "f ") {
			// We will start parsing with a string stream
			std::istringstream ss = std::istringstream(line.substr(2));

			// Our face starts as all zeros
			uivec3 face = { static_cast<uint32_t>(-1) };

			// Remove the first 2 characters (the 'f ')
			line = line.substr(2);
			// Trim any leftover whitespace characters
			rtrim(line);

			// Track whether we are on a whitespace char, and get the length of the line
			bool ws = false;
			size_t len = line.length();

			// Iterate over line, track where we the beginning of our integer is
			for (size_t ix = 0, start = 0, attribIx = 0, vertexIx = 0; ix < len; ix++) {
				// If we have a digit, set our whitespace tracker to false
				if (std::isdigit(line[ix])) {
					ws = false;
				}
				// If we are at a divisor character, or at the end of the string, we need to extract
				if ((line[ix] == '/' || line[ix] == '\\') | (ix == (len - 1))) {
					// Extract the number
					int index = atoi(line.c_str() + start);
					// Update our start offset for the next number
					start = ix + 1;
					if (attribIx == 0) {
						// Store the index (and adjust index to be 0-based)
						face[vertexIx] = index - 1;
					}
					// Advance to the next attribute
					attribIx++;
					ws = false;
				}
				// Handle whitespace characters, these indicate the start of a new face
				else if (line[ix] == ' ' || line[ix] == '\t') {
					// If the last character was not whitespace, we extract the last attribute
					if (!ws) {
						int index = atoi(line.c_str() + start);
						if (attribIx == 0) {
							face[vertexIx] = index - 1;
						}
						vertexIx++;
						attribIx = 0;
					}
					// Update the start index for attributes
					start = ix + 1;
					ws = true;
				}
			}
			// Insert the lines
			const uint64_t mask = 0x0000FFFF;
			uint64_t key;
			key = (face[0] & mask) << 32 | face[1];
			lines.insert(key);
			key = (face[1] & mask) << 32 | face[2];
			lines.insert(key);
			key = (face[2] & mask) << 32 | face[0];
			lines.insert(key);
		}

	}
	
	LineMesh::Sptr result = std::make_shared<LineMesh>();

	for(uint64_t key : lines) {
		uint32_t a = key >> 32;
		uint32_t b = key & 0x0000FFFF;
		result->AddLine(positions[a], positions[b], vec3::White);
	}

	return result;
}

template <typename T>
void Write(std::ofstream& stream, const T& value) {
	stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}

template <typename ValueType, typename IteratorType>
void Write(std::ofstream& stream, const IteratorType& begin, const IteratorType& end) {
	Write(stream, (uint64_t)(std::distance(begin, end)));
	IteratorType it = begin;
	while (it != end) {
		Write<ValueType>(stream, *it);
		++it;
	}
}

template <typename T, typename ... TArgs>
T Read(std::ifstream& stream, TArgs ... args) {
	T result(std::forward<TArgs>(args)...);
	stream.read(reinterpret_cast<char*>(&result), sizeof(T));
	return result;
}

template <typename T>
void Read(std::ifstream& stream, T& result) {
	stream.read(reinterpret_cast<char*>(&result), sizeof(T));
}

template <typename ValueType, typename ContainerType>
void Read(std::ifstream& stream, ContainerType& result, void(ContainerType::*pushOp)(const ValueType& v)) {
	const uint64_t size = Read<uint64_t>(stream);
	result.resize(size);
	for(uint64_t ix = 0; ix < size; ix++) {
		ValueType element = Read<ValueType>(stream);
		(result.*pushOp)(element);
	}
}

template <typename ValueType>
void ReadIterable(std::ifstream& stream, std::function<void(const ValueType&)> process) {
	const uint64_t size = Read<uint64_t>(stream);
	for (uint64_t ix = 0; ix < size; ix++) {
		ValueType element = Read<ValueType>(stream);
		process(element);
	}
}

void ObjLoader::SaveMeshBinary(const std::string& filename, const LineMesh::Sptr& mesh) {
	// Open our file in binary mode
	std::ofstream file;
	file.open(filename, std::ios::binary);

	// If our file fails to open, we will throw an error
	if (!file) {
		throw std::runtime_error("Failed to open file");
	}

	const char HeaderMagic[11] = "LineMesh";
	const uint16_t VersionCode = 0x00'01;	
	file.write(HeaderMagic, 11);
	Write(file, VersionCode);
	
	uint32_t meshSize = mesh->LineCount();
	Write(file, meshSize);

	// Extract unique vertices from mesh
	std::set<Vertex> uniqueVerts;
	for(size_t i = 0; i < meshSize; i++) {
		const Line& line = mesh->GetLine(i);
		uniqueVerts.insert({ line.Start, line.Color});
		uniqueVerts.insert({ line.End, line.Color });
	}

	// Recalculate keys for each line within the mesh
	std::set<uint64_t> uniqueLines;
	for (size_t i = 0; i < meshSize; i++) {
		const Line& line = mesh->GetLine(i);
		Vertex start = { line.Start, line.Color };
		Vertex end = { line.End, line.Color };
		uint32_t a = std::distance(uniqueVerts.begin(), std::find(uniqueVerts.begin(), uniqueVerts.end(), start));
		uint32_t b = std::distance(uniqueVerts.begin(), std::find(uniqueVerts.begin(), uniqueVerts.end(), end));
		const uint64_t mask = 0x0000FFFF;
		uint64_t key = (a & mask) << 32 | b;
		uniqueLines.insert(key);
	}

	// Iterate over vertices and write to file
	Write<Vertex>(file, uniqueVerts.begin(), uniqueVerts.end());

	// Iterate over keys and write to file
	Write<uint64_t>(file, uniqueLines.begin(), uniqueLines.end());

	file.close();
}

LineMesh::Sptr ObjLoader::LoadMeshBinary(const std::string& filename) {
	// Open our file in binary mode
	std::ifstream file;
	file.open(filename, std::ios::binary);

	// If our file fails to open, we will throw an error
	if (!file) {
		throw std::runtime_error("Failed to open file");
	}
	#pragma pack(push, 1)
	struct {
		char Magic[11];
		uint16_t VersionCode;
	} Header;
	#pragma pack(pop)
	Read(file, Header);
	uint32_t meshSize = Read<uint32_t>(file);
	uint64_t numVerts = Read<uint64_t>(file);
	std::vector<Vertex> verts;
	verts.reserve(numVerts);
	for(size_t ix = 0; ix < numVerts; ix++) {
		Vertex v = Read<Vertex>(file);
		verts.push_back(v);
	}

	LineMesh::Sptr result = std::make_shared<LineMesh>();
	ReadIterable<uint64_t>(file, [&](const uint64_t& k) {
		Line line;
		uint32_t a = k >> 32;
		uint32_t b = k & 0x0000FFFF;
		line.Color = verts[a].Color;
		line.Start = verts[a].Position;
		line.End   = verts[b].Position;
		result->AddLine(line);
	});

	file.close();
	
	return result;
}
