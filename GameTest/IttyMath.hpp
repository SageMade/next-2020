#pragma once
#include <cassert>
#include <memory>
#include <cmath>
#include <limits>

/*
 * General purpose header-only library for vector and matrix math
 * Author: Shawn Matthews
 * Date: April 17th, 2020
 *
 * TODO:
 *  - SIMD
 *  - Better matrices
 *  - Moar math functions
 */

#define CLIP_NEGATIVE_TO_ONE 0
#define CLIP_ZERO_TO_ONE 1

#ifndef MATH_CLIP_MODE
#define MATH_CLIP_MODE CLIP_NEGATIVE_TO_ONE
#endif

#undef max
#undef min

template <size_t Elements, typename BaseType>
struct vec { };

template <typename BaseType>
struct mat4_t;
template <typename BaseType>
struct mat3_t;

template <typename T = float>
constexpr T Pi() {
	static const T __value = static_cast<T>(3.1415926535897932384626433832795);
	return __value;
}
template <typename T = float>
constexpr T HPi() {
	static const T __value = static_cast<T>(1.5707963267948966192313216916398);
	return __value;
}
template <typename T = float>
constexpr T QPi() {
	static const T __value = static_cast<T>(0.78539816339744830961566084581988);
	return __value;
}
template <typename T = float>
constexpr T TwoPi() {
	static const T __value = static_cast<T>(6.283185307179586476925286766559);
	return __value;
}

template <typename T>
constexpr T VMax() { return std::numeric_limits<T>::max(); }
template <typename T>
constexpr T VMin() { return std::numeric_limits<T>::min(); }

#pragma region Swizzle Implementations

namespace impl
{
	constexpr bool CanAssign(size_t a, size_t b) { return a != b; };
	constexpr bool CannotAssign(size_t a, size_t b) { return a == b; };
	constexpr bool CanAssign(size_t a, size_t b, size_t c, size_t d) { return (a != b) & (a != c) & (a != d) & (b != c) & (b != d); };
	constexpr bool CannotAssign(size_t a, size_t b, size_t c, size_t d) { return !((a != b) & (a != c) & (a != d) & (b != c) & (b != d)); };
	constexpr bool CanAssign(size_t a, size_t b, size_t c) { return (a != b) & (a != c) & (b != c); };
	constexpr bool CannotAssign(size_t a, size_t b, size_t c) { return !((a != b) & (a != c) & (b != c)); };


	// 2D swizzles - Default	
	template <size_t vSize, size_t a, size_t b, typename BaseType, typename A = void>
	struct _swizzle2d {
		operator vec<2, BaseType>() { return vec<2, BaseType>(); }
		vec<2, BaseType>& operator=(const vec<2, BaseType>& other) { return *(vec<2, BaseType>*)this; }
		template <typename T, size_t oSize, size_t oA, size_t oB>
		vec<2, BaseType>& operator=(_swizzle2d<oSize, oA, oB, T> other) { return *(vec<2, BaseType>*)this; }
		template <typename T> vec<2, BaseType>& operator=(T other) { return *(vec<2, BaseType>*)this; }

		template<typename T> vec<2, BaseType>& operator+=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator-=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator*=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator/=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator%=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator&=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator|=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator^=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator<<=(const T& rhs) { return *(vec<2, BaseType>*)this; };
		template<typename T> vec<2, BaseType>& operator>>=(const T& rhs) { return *(vec<2, BaseType>*)this; };
	};

	// 2D Swizzles - Non-Assignable
	template <size_t vSize, size_t a, size_t b, typename BaseType>
	struct _swizzle2d<vSize, a, b, BaseType, std::enable_if_t<CannotAssign(a, b)>> {
	private:
		typedef vec<2, BaseType> vec2;
		float _v[vSize];

	public:
		operator vec2() { return vec2(_v[a], _v[b]); } vec2& operator=(const vec2& other) = delete;
		template <typename T, size_t oSize, size_t oA, size_t oB> vec2& operator=(_swizzle2d<oSize, oA, oB, T> other) = delete;
		template <typename T> vec2& operator=(T other) = delete;

		template<typename T> vec2& operator+=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator-=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator*=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator/=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator%=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator&=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator|=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator^=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator<<=(const vec<2, T>& rhs) = delete;
		template<typename T> vec2& operator>>=(const vec<2, T>& rhs) = delete;
	};
	
	// 2D Swizzles
	template <size_t vSize, size_t a, size_t b, typename BaseType>
	struct _swizzle2d<vSize, a, b, BaseType, std::enable_if_t<CanAssign(a, b)>> {
	private:
		template <typename FromType>
		static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }
		typedef vec<2, BaseType> vec2;
		float _v[vSize];
	public:
		template <typename T>
		vec2& operator=(const vec<2, T>& other) {
			_v[a] = __BT(other.X);
			_v[b] = __BT(other.Y);
			return *(vec<2, BaseType>*)this;
		}
		template <typename T>
		vec2& operator=(T rhs) {
			_v[a] = __BT(rhs);
			_v[b] = __BT(rhs);
			return *(vec<2, BaseType>*)this;
		}
		template <typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator=(const _swizzle2d<oSize, oA, oB, T>& other) {
			_v[a] = ((T*)&other)[oA];
			_v[b] = ((T*)&other)[oB];
			return *(vec<2, BaseType>*)this;
		}
		operator vec2() { return vec2(_v[a], _v[b]); }
		
		template<typename T>
		vec2& operator+=(const vec<2, T>& rhs) { _v[a] += __BT(rhs.X);	_v[b] += __BT(rhs.Y); return *(vec2*)this; }
		template<typename T>
		vec2& operator-=(const vec<2, T>& rhs) { _v[a] -= __BT(rhs.X); _v[b] -= __BT(rhs.Y); return *(vec2*)this; }
		template<typename T>
		vec2& operator*=(const vec<2, T>& rhs) { _v[a] *= __BT(rhs.X); _v[b] *= __BT(rhs.Y); return *(vec2*)this; }
		template<typename T>
		vec2& operator/=(const vec<2, T>& rhs) { _v[a] /= __BT(rhs.X); _v[b] /= __BT(rhs.Y); return *(vec2*)this; }
		template<typename T>
		vec2& operator%=(const vec<2, T>& rhs) { _v[a] %= rhs.X; _v[b] %= rhs.Y; return *(vec2*)this; }
		template<typename T>
		vec2& operator&=(const vec<2, T>& rhs) { _v[a] &= rhs.X; _v[b] &= rhs.Y; return *(vec2*)this; }
		template<typename T>
		vec2& operator|=(const vec<2, T>& rhs) { _v[a] |= rhs.X; _v[b] |= rhs.Y; return *(vec2*)this; }
		template<typename T>
		vec2& operator^=(const vec<2, T>& rhs) { _v[a] ^= rhs.X; _v[b] ^= rhs.Y; return *(vec2*)this; }
		template<typename T>
		vec2& operator<<=(const vec<2, T>& rhs) { _v[a] <<= rhs.X; _v[b] <<= rhs.Y; return *(vec2*)this; }
		template<typename T>
		vec2& operator>>=(const vec<2, T>& rhs) { _v[a] >>= rhs.X; _v[b] >>= rhs.Y; return *(vec2*)this; }
		template<typename T> vec2& operator+=(T rhs) { _v[a] += __BT(rhs); _v[b] += __BT(rhs); return *(vec2*)this; }
		template<typename T> vec2& operator-=(T rhs) { _v[a] -= __BT(rhs); _v[b] -= __BT(rhs); return *(vec2*)this; }
		template<typename T> vec2& operator*=(T rhs) { _v[a] *= __BT(rhs); _v[b] *= __BT(rhs); return *(vec2*)this; }
		template<typename T> vec2& operator/=(T rhs) { _v[a] /= __BT(rhs); _v[b] /= __BT(rhs); return *(vec2*)this; }
		template<typename T> vec2& operator%=(T rhs) { _v[a] %= rhs; _v[b] %= rhs; return *(vec2*)this; }
		template<typename T> vec2& operator&=(T rhs) { _v[a] &= rhs; _v[b] &= rhs; return *(vec2*)this; }
		template<typename T> vec2& operator|=(T rhs) { _v[a] |= rhs; _v[b] |= rhs; return *(vec2*)this; }
		template<typename T> vec2& operator^=(T rhs) { _v[a] ^= rhs; _v[b] ^= rhs; return *(vec2*)this; }
		template<typename T> vec2& operator<<=(T rhs) { _v[a] <<= rhs; _v[b] <<= rhs; return *(vec2*)this; }
		template<typename T> vec2& operator>>=(T rhs) { _v[a] >>= rhs; _v[b] >>= rhs; return *(vec2*)this; }
		
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator+=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] += __BT(((T*)&rhs)[oA]); _v[b] += __BT(((T*)&rhs)[oB]); return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator-=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] -= __BT(((T*)&rhs)[oA]); _v[b] -= __BT(((T*)&rhs)[oB]); return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator*=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] *= __BT(((T*)&rhs)[oA]); _v[b] *= __BT(((T*)&rhs)[oB]); return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator/=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] /= __BT(((T*)&rhs)[oA]); _v[b] /= __BT(((T*)&rhs)[oB]); return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator%=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] %= ((T*)&rhs)[oA]; _v[b] %= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator&=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] &= ((T*)&rhs)[oA]; _v[b] &= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator|=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] |= ((T*)&rhs)[oA]; _v[b] |= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator^=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] ^= ((T*)&rhs)[oA]; _v[b] ^= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator<<=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] <<= ((T*)&rhs)[oA]; _v[b] <<= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB>
		vec2& operator>>=(_swizzle2d<oSize, oA, oB, T> rhs) {
			_v[a] >>= ((T*)&rhs)[oA]; _v[b] >>= ((T*)&rhs)[oB]; return *(vec2*)this;
		}
	};

	// 3D swizzles - Default	
	template <size_t vSize, size_t a, size_t b, size_t c, typename BaseType, typename A =void>
	struct _swizzle3d {
		operator vec<3, BaseType>() { return vec<3, BaseType>(); }
		vec<3, BaseType>& operator=(const vec<3, BaseType>& other) { return *(vec<3, BaseType>*)this; }
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec<3, BaseType>& operator=(_swizzle3d<oSize, oA, oB, oC, T> other) { return *(vec<3, BaseType>*)this; }

		template<typename T> vec<3, BaseType>& operator+=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator-=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator*=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator/=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator%=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator&=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator|=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator^=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator<<=(const T& rhs) { return *(vec<3, BaseType>*)this; };
		template<typename T> vec<3, BaseType>& operator>>=(const T& rhs) { return *(vec<3, BaseType>*)this; };
	};

	// 3D Swizzles - Non-assignable
	template <size_t vSize, size_t a, size_t b, size_t c, typename BaseType>
	struct _swizzle3d<vSize, a, b, c, BaseType, std::enable_if_t<CannotAssign(a, b, c)>> {
	private:
		typedef vec<3, BaseType> vec3;
		float _v[vSize];
		
	public:
		operator vec3() { return vec3(_v[a], _v[b], _v[c]); }
		vec3& operator=(const vec3& other) = delete;
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator=(_swizzle3d<oSize, oA, oB, oC, T> other) = delete;

		template<typename T> vec3& operator+=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator-=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator*=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator/=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator%=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator&=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator|=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator^=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator<<=(const vec<3, T>& rhs) = delete;
		template<typename T> vec3& operator>>=(const vec<3, T>& rhs) = delete;
	};

	// 3D Swizzles - Primary
	template <size_t vSize, size_t a, size_t b, size_t c, typename BaseType>
	struct _swizzle3d<vSize, a, b, c, BaseType, std::enable_if_t<CanAssign(a, b, c)>>{
	private:
		template <typename FromType>
		static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }
		typedef vec<3, BaseType> vec3;
		float _v[vSize];

	public:
		operator vec3() { return vec3(_v[a], _v[b], _v[c]); }
		template<typename T>
		vec3& operator=(const vec<3, T>& other) {
			_v[a] = __BT(other.X);
			_v[b] = __BT(other.Y);
			_v[c] = __BT(other.Z);
			return *(vec<3, BaseType>*)this;
		}
		template<typename T>
		vec3& operator=(T rhs) {
			_v[a] = __BT(rhs);
			_v[b] = __BT(rhs);
			_v[c] = __BT(rhs);
			return *(vec<3, BaseType>*)this;
		}
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator=(_swizzle3d<oSize, oA, oB, oC, T> other) {
			_v[a] = ((T*)&other)[oA];
			_v[b] = ((T*)&other)[oB];
			_v[c] = ((T*)&other)[oC];
			return *(vec<3, BaseType>*)this;
		}
		template<typename T> vec3& operator+=(const vec<3, T>& rhs) {
			_v[a] += __BT(rhs.X);	_v[b] += __BT(rhs.Y); _v[c] += rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator-=(const vec<3, T>& rhs) {
			_v[a] -= __BT(rhs.X); _v[b] -= __BT(rhs.Y); _v[c] -= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator*=(const vec<3, T>& rhs) {
			_v[a] *= __BT(rhs.X); _v[b] *= __BT(rhs.Y); _v[c] *= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator/=(const vec<3, T>& rhs) {
			_v[a] /= __BT(rhs.X); _v[b] /= __BT(rhs.Y); _v[c] /= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator%=(const vec<3, T>& rhs) {
			_v[a] %= rhs.X; _v[b] %= rhs.Y; _v[c] %= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator&=(const vec<3, T>& rhs) {
			_v[a] &= rhs.X; _v[b] &= rhs.Y; _v[c] &= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator|=(const vec<3, T>& rhs) {
			_v[a] |= rhs.X; _v[b] |= rhs.Y; _v[c] |= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator^=(const vec<3, T>& rhs) {
			_v[a] ^= rhs.X; _v[b] ^= rhs.Y; _v[c] ^= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator<<=(const vec<3, T>& rhs) {
			_v[a] <<= rhs.X; _v[b] <<= rhs.Y; _v[c] <<= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator>>=(const vec<3, T>& rhs) {
			_v[a] >>= rhs.X; _v[b] >>= rhs.Y; _v[c] >>= rhs.Z; return *(vec3*)this;
		}
		template<typename T> vec3& operator+=(T rhs) { _v[a] += __BT(rhs); _v[b] += __BT(rhs); _v[c] += rhs; return *(vec3*)this; }
		template<typename T> vec3& operator-=(T rhs) { _v[a] -= __BT(rhs); _v[b] -= __BT(rhs); _v[c] -= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator*=(T rhs) { _v[a] *= __BT(rhs); _v[b] *= __BT(rhs); _v[c] *= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator/=(T rhs) { _v[a] /= __BT(rhs); _v[b] /= __BT(rhs); _v[c] /= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator%=(T rhs) { _v[a] %= rhs; _v[b] %= rhs; _v[c] %= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator&=(T rhs) { _v[a] &= rhs; _v[b] &= rhs; _v[c] &= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator|=(T rhs) { _v[a] |= rhs; _v[b] |= rhs; _v[c] |= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator^=(T rhs) { _v[a] ^= rhs; _v[b] ^= rhs; _v[c] ^= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator<<=(T rhs) { _v[a] <<= rhs; _v[b] <<= rhs; _v[c] <<= rhs; return *(vec3*)this; }
		template<typename T> vec3& operator>>=(T rhs) {	_v[a] >>= rhs; _v[b] >>= rhs; _v[c] >>= rhs; return *(vec3*)this; }

		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator+=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] += __BT(((T*)&rhs)[oA]); _v[b] += __BT(((T*)&rhs)[oB]); _v[c] += __BT(((T*)&rhs)[oC]); return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator-=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] -= __BT(((T*)&rhs)[oA]); _v[b] -= __BT(((T*)&rhs)[oB]); _v[c] -= __BT(((T*)&rhs)[oC]); return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator*=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] *= __BT(((T*)&rhs)[oA]); _v[b] *= __BT(((T*)&rhs)[oB]); _v[c] *= __BT(((T*)&rhs)[oC]); return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator/=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] /= __BT(((T*)&rhs)[oA]); _v[b] /= __BT(((T*)&rhs)[oB]); _v[c] /= __BT(((T*)&rhs)[oC]); return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator%=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] %= ((T*)&rhs)[oA]; _v[b] %= ((T*)&rhs)[oB]; _v[c] %= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator&=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] &= ((T*)&rhs)[oA]; _v[b] &= ((T*)&rhs)[oB]; _v[c] &= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator|=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] |= ((T*)&rhs)[oA]; _v[b] |= ((T*)&rhs)[oB]; _v[c] |= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator^=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] ^= ((T*)&rhs)[oA]; _v[b] ^= ((T*)&rhs)[oB]; _v[c] ^= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator<<=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] <<= ((T*)&rhs)[oA]; _v[b] <<= ((T*)&rhs)[oB]; _v[c] <<= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC>
		vec3& operator>>=(_swizzle3d<oSize, oA, oB, oC, T> rhs) {
			_v[a] >>= ((T*)&rhs)[oA]; _v[b] >>= ((T*)&rhs)[oB]; _v[c] >>= ((T*)&rhs)[oC]; return *(vec3*)this;
		}
	};
	
	
	// 4D swizzles - Default
	template <size_t vSize, size_t a, size_t b, size_t c, size_t d, typename BaseType, typename A = void>
	struct _swizzle4d {
		operator vec<4, BaseType>() { return vec<4, BaseType>(); }
		vec<4, BaseType>& operator=(const vec<3, BaseType>& other) { return *(vec<3, BaseType>*)this; }
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec<4, BaseType>& operator=(_swizzle4d<oSize, oA, oB, oC, oD, T> other) { return *(vec<3, BaseType>*)this; }
		
		template<typename T> vec<4, BaseType>& operator+=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator-=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator*=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator/=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator%=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator&=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator|=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator^=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator<<=(const T& rhs) { return *(vec<4, BaseType>*)this; };
		template<typename T> vec<4, BaseType>& operator>>=(const T& rhs) { return *(vec<4, BaseType>*)this; };
	};

	// 4D Swizzles - Non-assignable
	template <size_t vSize, size_t a, size_t b, size_t c, size_t d, typename BaseType>
	struct _swizzle4d<vSize, a, b, c, d, BaseType, std::enable_if_t<CannotAssign(a, b, c, d)>> {
	private:
		typedef vec<4, BaseType> vec4;
		float _v[vSize];
	public:
		operator vec4() { return vec4(_v[a], _v[b], _v[c], _v[d]); }
		vec4& operator=(const vec4& other) = delete;
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator=(_swizzle4d<oSize, oA, oB, oC, oD, T> other) = delete;

		template<typename T> vec<4, BaseType>& operator+=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator-=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator*=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator/=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator%=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator&=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator|=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator^=(const T& rhs)  = delete;
		template<typename T> vec<4, BaseType>& operator<<=(const T& rhs) = delete;
		template<typename T> vec<4, BaseType>& operator>>=(const T& rhs) = delete;
	};

	// 4D Swizzles - Primary
	template <size_t vSize, size_t a, size_t b, size_t c, size_t d, typename BaseType>
	struct _swizzle4d<vSize, a, b, c, d, BaseType, std::enable_if_t<CanAssign(a, b, c, d)>> {
	private:
		template <typename FromType>
		static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }
		typedef vec<4, BaseType> vec4;
		float _v[vSize];
	public:
		operator vec4() { return vec4(_v[a], _v[b], _v[c], _v[d]); }
		template<typename T>
		vec4& operator=(const vec<4, T>& other) {
			_v[a] = __BT(other.X);
			_v[b] = __BT(other.Y);
			_v[c] = __BT(other.Z);
			_v[d] = __BT(other.W);
			return *(vec<4, BaseType>*)this;
		}
		template<typename T>
		vec4& operator=(T rhs) {
			_v[a] = __BT(rhs);
			_v[b] = __BT(rhs);
			_v[c] = __BT(rhs);
			_v[d] = __BT(rhs);
			return *(vec<4, BaseType>*)this;
		}
		template <typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator=(_swizzle4d<oSize, oA, oB, oC, oD, T> other) {
			_v[a] = ((T*)&other)[oA];
			_v[b] = ((T*)&other)[oB];
			_v[c] = ((T*)&other)[oC];
			_v[d] = ((T*)&other)[oD];
			return *(vec<4, BaseType>*)this;
		}
		template<typename T> vec4& operator+=(const vec<4, T>& rhs) { _v[a] += __BT(rhs.X);	_v[b] += __BT(rhs.Y); _v[c] += rhs.Z; _v[d] += rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator-=(const vec<4, T>& rhs) { _v[a] -= __BT(rhs.X); _v[b] -= __BT(rhs.Y); _v[c] -= rhs.Z; _v[d] -= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator*=(const vec<4, T>& rhs) { _v[a] *= __BT(rhs.X); _v[b] *= __BT(rhs.Y); _v[c] *= rhs.Z; _v[d] *= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator/=(const vec<4, T>& rhs) { _v[a] /= __BT(rhs.X); _v[b] /= __BT(rhs.Y); _v[c] /= rhs.Z; _v[d] /= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator%=(const vec<4, T>& rhs) { _v[a] %= rhs.X; _v[b] %= rhs.Y; _v[c] %= rhs.Z; _v[d] %= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator&=(const vec<4, T>& rhs) { _v[a] &= rhs.X; _v[b] &= rhs.Y; _v[c] &= rhs.Z; _v[d] &= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator|=(const vec<4, T>& rhs) { _v[a] |= rhs.X; _v[b] |= rhs.Y; _v[c] |= rhs.Z; _v[d] |= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator^=(const vec<4, T>& rhs) { _v[a] ^= rhs.X; _v[b] ^= rhs.Y; _v[c] ^= rhs.Z; _v[d] ^= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator<<=(const vec<4, T>& rhs) { _v[a] <<= rhs.X; _v[b] <<= rhs.Y; _v[c] <<= rhs.Z; _v[d] <<= rhs.W; return *(vec4*)this; }
		template<typename T> vec4& operator>>=(const vec<4, T>& rhs) { _v[a] >>= rhs.X; _v[b] >>= rhs.Y; _v[c] >>= rhs.Z; _v[d] >>= rhs.W;  return *(vec4*)this; }
		template<typename T> vec4& operator+=(T rhs) { _v[a] += __BT(rhs); _v[b] += __BT(rhs); _v[c] += rhs; _v[d] += rhs; return *(vec4*)this; }
		template<typename T> vec4& operator-=(T rhs) { _v[a] -= __BT(rhs); _v[b] -= __BT(rhs); _v[c] -= rhs; _v[d] -= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator*=(T rhs) { _v[a] *= __BT(rhs); _v[b] *= __BT(rhs); _v[c] *= rhs; _v[d] *= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator/=(T rhs) { _v[a] /= __BT(rhs); _v[b] /= __BT(rhs); _v[c] /= rhs; _v[d] /= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator%=(T rhs) { _v[a] %= rhs; _v[b] %= rhs; _v[c] %= rhs; _v[d] %= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator&=(T rhs) { _v[a] &= rhs; _v[b] &= rhs; _v[c] &= rhs; _v[d] &= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator|=(T rhs) { _v[a] |= rhs; _v[b] |= rhs; _v[c] |= rhs; _v[d] |= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator^=(T rhs) { _v[a] ^= rhs; _v[b] ^= rhs; _v[c] ^= rhs; _v[d] ^= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator<<=(T rhs) { _v[a] <<= rhs; _v[b] <<= rhs; _v[c] <<= rhs; _v[d] <<= rhs;  return *(vec4*)this; }
		template<typename T> vec4& operator>>=(T rhs) { _v[a] >>= rhs; _v[b] >>= rhs; _v[c] >>= rhs; _v[d] >>= rhs;  return *(vec4*)this; }

		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator+=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] += __BT(((T*)&rhs)[oA]); _v[b] += __BT(((T*)&rhs)[oB]); _v[c] += __BT(((T*)&rhs)[oC]); _v[d] += __BT(((T*)&rhs)[oD]); return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator-=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] -= __BT(((T*)&rhs)[oA]); _v[b] -= __BT(((T*)&rhs)[oB]); _v[c] -= __BT(((T*)&rhs)[oC]); _v[d] -= __BT(((T*)&rhs)[oD]); return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator*=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] *= __BT(((T*)&rhs)[oA]); _v[b] *= __BT(((T*)&rhs)[oB]); _v[c] *= __BT(((T*)&rhs)[oC]); _v[d] *= __BT(((T*)&rhs)[oD]); return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator/=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] /= __BT(((T*)&rhs)[oA]); _v[b] /= __BT(((T*)&rhs)[oB]); _v[c] /= __BT(((T*)&rhs)[oC]); _v[d] /= __BT(((T*)&rhs)[oD]); return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator%=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] %= ((T*)&rhs)[oA]; _v[b] %= ((T*)&rhs)[oB]; _v[c] %= ((T*)&rhs)[oC]; _v[d] %= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator&=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] &= ((T*)&rhs)[oA]; _v[b] &= ((T*)&rhs)[oB]; _v[c] &= ((T*)&rhs)[oC]; _v[d] &= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator|=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] |= ((T*)&rhs)[oA]; _v[b] |= ((T*)&rhs)[oB]; _v[c] |= ((T*)&rhs)[oC]; _v[d] |= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator^=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] ^= ((T*)&rhs)[oA]; _v[b] ^= ((T*)&rhs)[oB]; _v[c] ^= ((T*)&rhs)[oC]; _v[d] ^= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator<<=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] <<= ((T*)&rhs)[oA]; _v[b] <<= ((T*)&rhs)[oB]; _v[c] <<= ((T*)&rhs)[oC]; _v[d] <<= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
		template<typename T, size_t oSize, size_t oA, size_t oB, size_t oC, size_t oD>
		vec4& operator>>=(_swizzle4d<oSize, oA, oB, oC, oD, T> rhs) {
			_v[a] >>= ((T*)&rhs)[oA]; _v[b] >>= ((T*)&rhs)[oB]; _v[c] >>= ((T*)&rhs)[oC]; _v[d] >>= ((T*)&rhs)[oD]; return *(vec4*)this;
		}
	};
}

#pragma endregion

#pragma region vec2

template<typename BaseType>
struct vec<2, BaseType>
{
private:
	template <typename FromType>
	static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }
	
	inline static const BaseType __0 = __BT(0.0);
	inline static const BaseType __1 = __BT(1.0);

	typedef vec<3, BaseType> vec3;
	typedef vec<2, BaseType> vec2;
	typedef vec<1, BaseType> vec1;
	
public:
	typedef BaseType        _ValueType;
	typedef vec<2,BaseType> _Type;
	typedef vec<2, bool>    _BoolType;
	
	union {
		BaseType  Values[2];
		struct { BaseType X, Y; };
		struct { BaseType R, G; };
		struct { BaseType U, V; };
		impl::_swizzle2d<2, 0, 1, BaseType> XX;
		impl::_swizzle2d<2, 0, 1, BaseType> XY;
		impl::_swizzle2d<2, 1, 0, BaseType> YX;
		impl::_swizzle2d<2, 1, 0, BaseType> YY;
	};

	
	vec() : X(__0), Y(__0) {}
	vec(BaseType value) : X(value), Y(value) {}
	vec(BaseType x, BaseType y) : X(x), Y(y) {}
	
	template <typename T>
	vec(const vec<3, T>& other) : X(other.X), Y(other.Y) {}
	template <typename T>
	vec(const vec<4, T>& other) : X(other.X), Y(other.Y) {}
	
	_Type& operator=(const _Type& v) = default;
	
	// Unary Arithmetic	
	template<typename T> inline constexpr _Type& operator +=(const vec<2, T>& r) { X += _BT(r.X); Y += _BT(r.Y); return *this; }
	template<typename T> inline constexpr _Type& operator +=(T r) { X += _BT(r); Y += _BT(r);return *this; }
	template<typename T> inline constexpr _Type& operator -=(const vec<2, T>& r) { X -= _BT(r.X); Y -= _BT(r.Y); return *this; }
	template<typename T> inline constexpr _Type& operator -=(T r) { X -= _BT(r); Y -= _BT(r);return *this; }
	template<typename T> inline constexpr _Type& operator *=(const vec<2, T>& r) { X *= _BT(r.X); Y *= _BT(r.Y); return *this; }
	template<typename T> inline constexpr _Type& operator *=(T r) { X *= _BT(r); Y *= _BT(r);return *this; }
	template<typename T> inline constexpr _Type& operator /=(const vec<2, T>& r) { X /= _BT(r.X); Y /= _BT(r.Y); return *this; }
	template<typename T> inline constexpr _Type& operator /=(T r) { X /= _BT(r); Y /= _BT(r);return *this; }

	// Increment and Decrement	
	inline constexpr _Type& operator ++() { ++X; ++Y; return *this; }
	inline constexpr _Type& operator --() { --X; --Y; return *this; }
	inline constexpr _Type operator ++(int) { return _Type(X + __BT(1), Y + __BT(1)); }
	inline constexpr _Type operator --(int) { return _Type(X - __BT(1), Y - __BT(1)); }

	// Unary bit operators
	template<typename T> inline constexpr _Type& operator%=(T value) { X %= value; Y %= value; return *this; }
	template<typename T> inline constexpr _Type& operator%=(const vec<2, T> value) { X %= value.X; Y %= value.Y; return *this; }
	template<typename T> inline constexpr _Type& operator&=(T value) { X &= value; Y &= value; return *this; }
	template<typename T> inline constexpr _Type& operator&=(const vec<2, T> value) { X &= value.X; Y &= value.Y; return *this; }
	template<typename T> inline constexpr _Type& operator|=(T value) { X |= value; Y |= value; return *this; }
	template<typename T> inline constexpr _Type& operator|=(const vec<2, T> value) { X |= value.X; Y |= value.Y; return *this; }
	template<typename T> inline constexpr _Type& operator^=(T value) { X ^= value; Y ^= value; return *this; }
	template<typename T> inline constexpr _Type& operator^=(const vec<2, T> value) { X ^= value.X; Y ^= value.Y; return *this; }
	template<typename T> inline constexpr _Type& operator<<=(T value) { X <<= value; Y <<= value; return *this; }
	template<typename T> inline constexpr _Type& operator<<=(const vec<2, T> value) { X <<= value.X; Y <<= value.Y; return *this; }
	template<typename T> inline constexpr _Type& operator>>=(T value) { X >>= value; Y >>= value; return *this; }
	template<typename T> inline constexpr _Type& operator>>=(const vec<2, T> value) { X >>= value.X; Y >>= value.Y; return *this; }
		
	inline BaseType operator[](size_t i) const { assert(i < 2); return Values[i]; }
	inline BaseType& operator[](size_t i) { assert(i < 2); return Values[i]; }

	vec<2, BaseType> GetNormal() const { return *this * InvSqrt(X*X + Y*Y); }
	vec<2, BaseType>& Normalize() { *this *= InvSqrt(X * X + Y * Y); return *this; }
};

// Unary operators
template <typename BaseType>
inline constexpr vec<2, BaseType> operator +(const vec<2, BaseType>& v) { return v; }
template <typename BaseType>
inline constexpr vec<2, BaseType> operator -(const vec<2, BaseType>& v) { return vec<2, BaseType>(-v.X, -v.Y); }
template <typename BaseType>
inline constexpr vec<2, BaseType> operator ~(const vec<2, BaseType>& v) { return vec<2, BaseType>(~v.X, ~v.Y); }

// Equality Operators
template <typename T1, typename T2>
inline constexpr bool operator ==(const vec<2, T1>& a, const vec<2, T2>& b) { return a.X == b.X & a.Y == b.Y; }
template <typename T1, typename T2>
inline constexpr bool operator !=(const vec<2, T1>& a, const vec<2, T2>& b) { return !(a.X == b.X & a.Y == b.Y); }
template <typename T1, typename T2>
inline constexpr vec<2, bool> operator &&(const vec<2, T1>& a, const vec<2, T2>& b) { return vec<2, bool>(a.X && b.X, a.Y && b.Y); }
template <typename T1, typename T2>
inline constexpr vec<2, bool> operator ||(const vec<2, T1>& a, const vec<2, T2>& b) { return vec<2, bool>(a.X || b.X, a.Y || b.Y); }

template <typename BaseType>
inline vec<2, BaseType> operator +(const vec<2, BaseType>& l, const vec<2, BaseType>& r) { return vec<2, BaseType>(l.X + r.X, l.Y + r.Y); }
template <typename BaseType>
inline vec<2, BaseType> operator -(const vec<2, BaseType>& l, const vec<2, BaseType>& r) { return _Type(l.X - r.X, l.Y - r.Y); }
template <typename BaseType>
inline vec<2, BaseType> operator *(const vec<2, BaseType>& l, const vec<2, BaseType>& r) { return _Type(l.X * r.X, l.Y * r.Y); }
template <typename BaseType>
inline vec<2, BaseType> operator /(const vec<2, BaseType>& l, const vec<2, BaseType>& r) { return vec<2, BaseType>(l.X / r.X, l.Y / r.Y); }
template <typename BaseType>
inline vec<2, BaseType> operator %(const vec<2, BaseType>& l, const vec<2, BaseType>& r) { return vec<2, BaseType>(Mod(l.X, r.X), Mod(l.Y, r.Y)); }


template <typename BaseType>
inline vec<2, BaseType> operator +(const vec<2, BaseType>& l, BaseType r) { return vec<2, BaseType>(l.X + r, l.Y + r); }
template <typename BaseType>
inline vec<2, BaseType> operator -(const vec<2, BaseType>& l, BaseType r) { return vec<2, BaseType>(l.X - r, l.Y - r); }
template <typename BaseType>
inline vec<2, BaseType> operator *(const vec<2, BaseType>& l, BaseType r) { return vec<2, BaseType>(l.X * r, l.Y * r); }
template <typename BaseType>
inline vec<2, BaseType> operator /(const vec<2, BaseType>& l, BaseType r) { return vec<2, BaseType>(l.X / r, l.Y / r); }
template <typename BaseType>
inline vec<2, BaseType> operator %(const vec<2, BaseType>& l, BaseType r) { return vec<2, BaseType>(Mod(l.X, r), Mod(l.Y, r)); }
template <typename BaseType>
inline vec<2, BaseType> operator +(BaseType r, const BaseType& l) { return vec<2, BaseType>(l.X + r, l.Y + r); }
template <typename BaseType>
inline vec<2, BaseType> operator -(BaseType r, const BaseType& l) { return vec<2, BaseType>(l.X - r, l.Y - r); }
template <typename BaseType>
inline vec<2, BaseType> operator *(BaseType r, const BaseType& l) { return vec<2, BaseType>(l.X * r, l.Y * r); }
template <typename BaseType>
inline vec<2, BaseType> operator /(BaseType r, const BaseType& l) { return vec<2, BaseType>(l.X / r, l.Y / r); }
template <typename BaseType>
inline vec<2, BaseType> operator %(BaseType r, const vec<2, BaseType>& l) { return vec<2, BaseType>(Mod(l.X, r), Mod(l.Y, r)); }

#pragma endregion

#pragma region vec3

template<typename BaseType>
struct vec<3, BaseType>
{
private:
	
	template <typename FromType>
	static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }

	inline static const BaseType __0 = __BT(0.0);
	inline static const BaseType __1 = __BT(1.0);

	typedef vec<3, BaseType> vec3;
	typedef vec<2, BaseType> vec2;
	typedef vec<1, BaseType> vec1;

public:
	typedef BaseType         _ValueType;
	typedef vec<3, BaseType> _Type;
	typedef vec<3, bool>     _BoolType;

	static const _Type Zero;
	static const _Type One;
	static const _Type White;
	static const _Type Black;
	static const _Type Gray;
	static const _Type Red;
	static const _Type Green;
	static const _Type Blue;
	static const _Type Yellow;
	static const _Type Purple;
	static const _Type Cyan;
	
	union {
		BaseType  Values[3];
		struct { BaseType X, Y, Z; };
		struct { BaseType R, G, B; };

		#pragma region Swizzles

		impl::_swizzle3d<3, 0, 0, 0, BaseType> XXX;
		impl::_swizzle3d<3, 0, 0, 1, BaseType> XXY;
		impl::_swizzle3d<3, 0, 0, 2, BaseType> XXZ;
		impl::_swizzle3d<3, 0, 1, 0, BaseType> XYX;
		impl::_swizzle3d<3, 0, 1, 1, BaseType> XYY;
		impl::_swizzle3d<3, 0, 1, 2, BaseType> XYZ;
		impl::_swizzle3d<3, 0, 2, 0, BaseType> XZX;
		impl::_swizzle3d<3, 0, 2, 1, BaseType> XZY;
		impl::_swizzle3d<3, 0, 2, 2, BaseType> XZZ;
		impl::_swizzle3d<3, 1, 0, 0, BaseType> YXX;
		impl::_swizzle3d<3, 1, 0, 1, BaseType> YXY;
		impl::_swizzle3d<3, 1, 0, 2, BaseType> YXZ;
		impl::_swizzle3d<3, 1, 1, 0, BaseType> YYX;
		impl::_swizzle3d<3, 1, 1, 1, BaseType> YYY;
		impl::_swizzle3d<3, 1, 1, 2, BaseType> YYZ;
		impl::_swizzle3d<3, 1, 2, 0, BaseType> YZX;
		impl::_swizzle3d<3, 1, 2, 1, BaseType> YZY;
		impl::_swizzle3d<3, 1, 2, 2, BaseType> YZZ;
		impl::_swizzle3d<3, 2, 0, 0, BaseType> ZXX;
		impl::_swizzle3d<3, 2, 0, 1, BaseType> ZXY;
		impl::_swizzle3d<3, 2, 0, 2, BaseType> ZXZ;
		impl::_swizzle3d<3, 2, 1, 0, BaseType> ZYX;
		impl::_swizzle3d<3, 2, 1, 1, BaseType> ZYY;
		impl::_swizzle3d<3, 2, 1, 2, BaseType> ZYZ;
		impl::_swizzle3d<3, 2, 2, 0, BaseType> ZZX;
		impl::_swizzle3d<3, 2, 2, 1, BaseType> ZZY;
		impl::_swizzle3d<3, 2, 2, 2, BaseType> ZZZ;

		impl::_swizzle2d<3, 0, 0, BaseType> XX;
		impl::_swizzle2d<3, 0, 1, BaseType> XY;
		impl::_swizzle2d<3, 0, 2, BaseType> XZ;
		impl::_swizzle2d<3, 1, 0, BaseType> YX;
		impl::_swizzle2d<3, 1, 1, BaseType> YY;
		impl::_swizzle2d<3, 1, 2, BaseType> YZ;
		impl::_swizzle2d<3, 2, 0, BaseType> ZX;
		impl::_swizzle2d<3, 2, 1, BaseType> ZY;
		impl::_swizzle2d<3, 2, 2, BaseType> ZZ;
		
		#pragma endregion
	};

	vec() : X(__0), Y(__0), Z(__0) {}
	vec(BaseType value) : X(value), Y(value), Z(value){}
	vec(BaseType x, BaseType y) : X(x), Y(y), Z(__0) {}
	vec(BaseType x, BaseType y, BaseType z) : X(x), Y(y), Z(z){}
	template <typename T>
	vec(const vec<2, T>& other) : X(other.X), Y(other.Y), Z(__0) { }
	template <typename T>
	vec(const vec<4, T>& other) : X(other.X), Y(other.Y), Z(other.Z) { }

	_Type& operator=(const _Type& v) = default;

	// Unary Arithmetic	
	template<typename T> inline constexpr _Type& operator +=(const vec<3, T> & r) { X += _BT(r.X); Y += _BT(r.Y); Z += _BT(r.Z); return *this; }
	template<typename T> inline constexpr _Type& operator +=(T r) { X += _BT(r); Y += _BT(r); Z += _BT(r); return *this; }
	template<typename T> inline constexpr _Type& operator -=(const vec<3, T> & r) { X -= _BT(r.X); Y -= _BT(r.Y); Z -= _BT(r.Z); return *this; }
	template<typename T> inline constexpr _Type& operator -=(T r) { X -= _BT(r); Y -= _BT(r); Z += _BT(r); return *this; }
	template<typename T> inline constexpr _Type& operator *=(const vec<3, T> & r) { X *= _BT(r.X); Y *= _BT(r.Y); Z *= _BT(r.Z); return *this; }
	template<typename T> inline constexpr _Type& operator *=(T r) { X *= _BT(r); Y *= _BT(r);  Z *= _BT(r); return *this; }
	template<typename T> inline constexpr _Type& operator /=(const vec<3, T> & r) { X /= _BT(r.X); Y /= _BT(r.Y); Z /= _BT(r.Z); return *this; }
	template<typename T> inline constexpr _Type& operator /=(T r) { X /= _BT(r); Y /= _BT(r);  Z /= _BT(r); return *this; }

	// Increment and Decrement	
	inline constexpr _Type& operator ++() { ++X; ++Y; ++Z; return *this; }
	inline constexpr _Type& operator --() { --X; --Y; --Z; return *this; }
	inline constexpr _Type operator ++(int) { return _Type(X + __BT(1), Y + __BT(1), Z + __BT(1)); }
	inline constexpr _Type operator --(int) { return _Type(X - __BT(1), Y - __BT(1), Z - __BT(1)); }

	// Unary bit operators
	template<typename T> inline constexpr _Type& operator%=(T value)
		{ X %= __BT(value); Y %= __BT(value); Z %= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator%=(const vec<3, T> value)
		{ X %= __BT(value.X); Y %= __BT(value.Y); Z %= __BT(value.Z); return *this; }
	template<typename T> inline constexpr _Type& operator&=(T value)
		{ X &= __BT(value); Y &= __BT(value); Z %= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator&=(const vec<3, T> value)
		{ X &= __BT(value.X); Y &= __BT(value.Y); Z &= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator|=(T value)
		{ X |= __BT(value); Y |= __BT(value); Z |= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator|=(const vec<3, T> value)
		{ X |= __BT(value.X); Y |= __BT(value.Y); Z |= __BT(value.Z); return *this; }
	template<typename T> inline constexpr _Type& operator^=(T value)
		{ X ^= __BT(value); Y ^= __BT(value); Z ^= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator^=(const vec<3, T> value)
		{ X ^= __BT(value.X); Y ^= __BT(value.Y); Z ^= __BT(value.Z); return *this; }
	template<typename T> inline constexpr _Type& operator<<=(T value)
		{ X <<= __BT(value); Y <<= __BT(value); Z <<= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator<<=(const vec<3, T> value)
		{ X <<= __BT(value.X); Y <<= __BT(value.Y); Z <<= __BT(value.Z); return *this; }
	template<typename T> inline constexpr _Type& operator>>=(T value)
		{ X >>= __BT(value); Y >>= __BT(value); Z >>= __BT(value); return *this; }
	template<typename T> inline constexpr _Type& operator>>=(const vec<3, T> value)
		{ X >>= __BT(value.X); Y >>= __BT(value.Y); Z >>= __BT(value.Z); return *this; }

	inline BaseType operator[](size_t i) const { assert(i < 3); return Values[i]; }
	inline BaseType& operator[](size_t i) { assert(i < 3); return Values[i]; }

	vec<3, BaseType> GetNormal() const { return *this * InvSqrt(X * X + Y * Y + Z * Z); }
	vec<3, BaseType>& Normalize() { *this *= InvSqrt(X * X + Y * Y + Z * Z); return *this; }
};

// Unary operators
template <typename BaseType>
inline constexpr vec<3, BaseType> operator +(const vec<3, BaseType>& v) { return v; }
template <typename BaseType>
inline constexpr vec<3, BaseType> operator -(const vec<3, BaseType>& v) { return vec<2, BaseType>(-v.X, -v.Y, -v.Z); }
template <typename BaseType>
inline constexpr vec<3, BaseType> operator ~(const vec<3, BaseType>& v) { return vec<2, BaseType>(~v.X, ~v.Y, ~v.Z); }

// Equality Operators
template <typename T1, typename T2>
inline constexpr bool operator ==(const vec<3, T1>& a, const vec<3, T2>& b) { return a.X == b.X & a.Y == b.Y & a.Z == b.Z; }
template <typename T1, typename T2>
inline constexpr bool operator !=(const vec<3, T1>& a, const vec<3, T2>& b) { return !(a.X == b.X & a.Y == b.Y & a.Z == b.Z); }
template <typename T1, typename T2>
inline constexpr vec<3, bool> operator &&(const vec<3, T1>& a, const vec<3, T2>& b) { return vec<3, bool>(a.X && b.X, a.Y && b.Y, a.Z && b.Z); }
template <typename T1, typename T2>
inline constexpr vec<3, bool> operator ||(const vec<3, T1>& a, const vec<3, T2>& b) { return vec<3, bool>(a.X || b.X, a.Y || b.Y, a.Z || b.Z); }

template <typename BaseType>
inline vec<3, BaseType> operator +(const vec<3, BaseType>& l, const vec<3, BaseType>& r) { return vec<3, BaseType>(l.X + r.X, l.Y + r.Y, l.Z + r.Z); }
template <typename BaseType>
inline vec<3, BaseType> operator -(const vec<3, BaseType>& l, const vec<3, BaseType>& r) { return vec<3, BaseType>(l.X - r.X, l.Y - r.Y, l.Z - r.Z); }
template <typename BaseType>
inline vec<3, BaseType> operator *(const vec<3, BaseType>& l, const vec<3, BaseType>& r) { return vec<3, BaseType>(l.X * r.X, l.Y * r.Y, l.Z * r.Z); }
template <typename BaseType>
inline vec<3, BaseType> operator /(const vec<3, BaseType>& l, const vec<3, BaseType>& r) { return vec<3, BaseType>(l.X / r.X, l.Y / r.Y, l.Z / r.Z); }
template <typename BaseType>
inline vec<3, BaseType> operator %(const vec<3, BaseType>& l, const vec<3, BaseType>& r) { return vec<3, BaseType>(Mod(l.X, r.X), Mod(l.Y, r.Y), Mod(l.Z, r.Z)); }

template <typename BaseType>
inline vec<3, BaseType> operator +(const vec<3, BaseType>& l, BaseType r) { return vec<3, BaseType>(l.X + r, l.Y + r, l.Z + r); }
template <typename BaseType>
inline vec<3, BaseType> operator -(const vec<3, BaseType>& l, BaseType r) { return vec<3, BaseType>(l.X - r, l.Y - r, l.Z - r); }
template <typename BaseType>
inline vec<3, BaseType> operator *(const vec<3, BaseType>& l, BaseType r) { return vec<3, BaseType>(l.X * r, l.Y * r, l.Z * r); }
template <typename BaseType>
inline vec<3, BaseType> operator /(const vec<3, BaseType>& l, BaseType r) { return vec<3, BaseType>(l.X / r, l.Y / r, l.Z / r); }
template <typename BaseType>
inline vec<3, BaseType> operator %(const vec<3, BaseType>& l, BaseType r) { return vec<3, BaseType>(Mod(l.X, r), Mod(l.Y, r), Mod(l.Z, r)); }
template <typename BaseType>
inline vec<3, BaseType> operator +(BaseType r, const vec<3, BaseType>& l) { return vec<3, BaseType>(l.X + r, l.Y + r, l.Z + r); }
template <typename BaseType>
inline vec<3, BaseType> operator -(BaseType r, const vec<3, BaseType>& l) { return vec<3, BaseType>(l.X - r, l.Y - r, l.Z - r); }
template <typename BaseType>
inline vec<3, BaseType> operator *(BaseType r, const vec<3, BaseType>& l) { return vec<3, BaseType>(l.X * r, l.Y * r, l.Z * r); }
template <typename BaseType>
inline vec<3, BaseType> operator /(BaseType r, const vec<3, BaseType>& l) { return vec<3, BaseType>(l.X / r, l.Y / r, l.Z / r); }
template <typename BaseType>
inline vec<3, BaseType> operator %(BaseType r, const vec<3, BaseType>& l) { return vec<3, BaseType>(Mod(l.X, r), Mod(l.Y, r), Mod(l.Z, r)); }

#pragma endregion

#pragma region vec4

template<typename BaseType>
struct vec<4, BaseType>
{
private:
	template <typename FromType>
	static constexpr BaseType __BT(FromType v) { return static_cast<BaseType>(v); }

	inline static const BaseType __0 = __BT(0.0);
	inline static const BaseType __1 = __BT(1.0);

	typedef vec<3, BaseType> vec3;
	typedef vec<2, BaseType> vec2;
	typedef vec<1, BaseType> vec1;

public:
	typedef BaseType         _ValueType;
	typedef vec<4, BaseType> _Type;
	typedef vec<4, bool>     _BoolType;

	static const _Type Zero;
	static const _Type One;
	static const _Type White;
	static const _Type TransparentWhite;
	static const _Type Black;
	static const _Type TransparentBlack;
	static const _Type Gray;
	static const _Type Red;
	static const _Type Green;
	static const _Type Blue;
	static const _Type Yellow;
	static const _Type Purple;
	static const _Type Cyan;

	union {
		BaseType  Values[4];
		struct { BaseType X, Y, Z, W; };
		struct { BaseType R, G, B, A; };

		#pragma region Swizzles

		impl::_swizzle4d<4, 0, 0, 0, 0, BaseType> XXXX;
		impl::_swizzle4d<4, 0, 0, 0, 1, BaseType> XXXY;
		impl::_swizzle4d<4, 0, 0, 0, 2, BaseType> XXXZ;
		impl::_swizzle4d<4, 0, 0, 0, 3, BaseType> XXXW;
		impl::_swizzle4d<4, 0, 0, 1, 0, BaseType> XXYX;
		impl::_swizzle4d<4, 0, 0, 1, 1, BaseType> XXYY;
		impl::_swizzle4d<4, 0, 0, 1, 2, BaseType> XXYZ;
		impl::_swizzle4d<4, 0, 0, 1, 3, BaseType> XXYW;
		impl::_swizzle4d<4, 0, 0, 2, 0, BaseType> XXZX;
		impl::_swizzle4d<4, 0, 0, 2, 1, BaseType> XXZY;
		impl::_swizzle4d<4, 0, 0, 2, 2, BaseType> XXZZ;
		impl::_swizzle4d<4, 0, 0, 2, 3, BaseType> XXZW;
		impl::_swizzle4d<4, 0, 0, 3, 0, BaseType> XXWX;
		impl::_swizzle4d<4, 0, 0, 3, 1, BaseType> XXWY;
		impl::_swizzle4d<4, 0, 0, 3, 2, BaseType> XXWZ;
		impl::_swizzle4d<4, 0, 0, 3, 3, BaseType> XXWW;
		impl::_swizzle4d<4, 0, 1, 0, 1, BaseType> XYXY;
		impl::_swizzle4d<4, 0, 1, 0, 2, BaseType> XYXZ;
		impl::_swizzle4d<4, 0, 1, 0, 3, BaseType> XYXW;
		impl::_swizzle4d<4, 0, 1, 1, 0, BaseType> XYYX;
		impl::_swizzle4d<4, 0, 1, 1, 1, BaseType> XYYY;
		impl::_swizzle4d<4, 0, 1, 1, 2, BaseType> XYYZ;
		impl::_swizzle4d<4, 0, 1, 1, 3, BaseType> XYYW;
		impl::_swizzle4d<4, 0, 1, 2, 0, BaseType> XYZX;
		impl::_swizzle4d<4, 0, 1, 2, 1, BaseType> XYZY;
		impl::_swizzle4d<4, 0, 1, 2, 2, BaseType> XYZZ;
		impl::_swizzle4d<4, 0, 1, 2, 3, BaseType> XYZW;
		impl::_swizzle4d<4, 0, 1, 3, 0, BaseType> XYWX;
		impl::_swizzle4d<4, 0, 1, 3, 1, BaseType> XYWY;
		impl::_swizzle4d<4, 0, 1, 3, 2, BaseType> XYWZ;
		impl::_swizzle4d<4, 0, 1, 3, 3, BaseType> XYWW;
		impl::_swizzle4d<4, 0, 2, 0, 1, BaseType> XZXY;
		impl::_swizzle4d<4, 0, 2, 0, 2, BaseType> XZXZ;
		impl::_swizzle4d<4, 0, 2, 0, 3, BaseType> XZXW;
		impl::_swizzle4d<4, 0, 2, 1, 0, BaseType> XZYX;
		impl::_swizzle4d<4, 0, 2, 1, 1, BaseType> XZYY;
		impl::_swizzle4d<4, 0, 2, 1, 2, BaseType> XZYZ;
		impl::_swizzle4d<4, 0, 2, 1, 3, BaseType> XZYW;
		impl::_swizzle4d<4, 0, 2, 2, 0, BaseType> XZZX;
		impl::_swizzle4d<4, 0, 2, 2, 1, BaseType> XZZY;
		impl::_swizzle4d<4, 0, 2, 2, 2, BaseType> XZZZ;
		impl::_swizzle4d<4, 0, 2, 2, 3, BaseType> XZZW;
		impl::_swizzle4d<4, 0, 2, 3, 0, BaseType> XZWX;
		impl::_swizzle4d<4, 0, 2, 3, 1, BaseType> XZWY;
		impl::_swizzle4d<4, 0, 2, 3, 2, BaseType> XZWZ;
		impl::_swizzle4d<4, 0, 2, 3, 3, BaseType> XZWW;
		impl::_swizzle4d<4, 0, 3, 0, 1, BaseType> XWXY;
		impl::_swizzle4d<4, 0, 3, 0, 2, BaseType> XWXZ;
		impl::_swizzle4d<4, 0, 3, 0, 3, BaseType> XWXW;
		impl::_swizzle4d<4, 0, 3, 1, 0, BaseType> XWYX;
		impl::_swizzle4d<4, 0, 3, 1, 1, BaseType> XWYY;
		impl::_swizzle4d<4, 0, 3, 1, 2, BaseType> XWYZ;
		impl::_swizzle4d<4, 0, 3, 1, 3, BaseType> XWYW;
		impl::_swizzle4d<4, 0, 3, 2, 0, BaseType> XWZX;
		impl::_swizzle4d<4, 0, 3, 2, 1, BaseType> XWZY;
		impl::_swizzle4d<4, 0, 3, 2, 2, BaseType> XWZZ;
		impl::_swizzle4d<4, 0, 3, 2, 3, BaseType> XWZW;
		impl::_swizzle4d<4, 0, 3, 3, 0, BaseType> XWWX;
		impl::_swizzle4d<4, 0, 3, 3, 1, BaseType> XWWY;
		impl::_swizzle4d<4, 0, 3, 3, 2, BaseType> XWWZ;
		impl::_swizzle4d<4, 0, 3, 3, 3, BaseType> XWWW;
		impl::_swizzle4d<4, 1, 0, 0, 1, BaseType> YXXY;
		impl::_swizzle4d<4, 1, 0, 0, 2, BaseType> YXXZ;
		impl::_swizzle4d<4, 1, 0, 0, 3, BaseType> YXXW;
		impl::_swizzle4d<4, 1, 0, 1, 0, BaseType> YXYX;
		impl::_swizzle4d<4, 1, 0, 1, 1, BaseType> YXYY;
		impl::_swizzle4d<4, 1, 0, 1, 2, BaseType> YXYZ;
		impl::_swizzle4d<4, 1, 0, 1, 3, BaseType> YXYW;
		impl::_swizzle4d<4, 1, 0, 2, 0, BaseType> YXZX;
		impl::_swizzle4d<4, 1, 0, 2, 1, BaseType> YXZY;
		impl::_swizzle4d<4, 1, 0, 2, 2, BaseType> YXZZ;
		impl::_swizzle4d<4, 1, 0, 2, 3, BaseType> YXZW;
		impl::_swizzle4d<4, 1, 0, 3, 0, BaseType> YXWX;
		impl::_swizzle4d<4, 1, 0, 3, 1, BaseType> YXWY;
		impl::_swizzle4d<4, 1, 0, 3, 2, BaseType> YXWZ;
		impl::_swizzle4d<4, 1, 0, 3, 3, BaseType> YXWW;
		impl::_swizzle4d<4, 1, 1, 0, 1, BaseType> YYXY;
		impl::_swizzle4d<4, 1, 1, 0, 2, BaseType> YYXZ;
		impl::_swizzle4d<4, 1, 1, 0, 3, BaseType> YYXW;
		impl::_swizzle4d<4, 1, 1, 1, 0, BaseType> YYYX;
		impl::_swizzle4d<4, 1, 1, 1, 1, BaseType> YYYY;
		impl::_swizzle4d<4, 1, 1, 1, 2, BaseType> YYYZ;
		impl::_swizzle4d<4, 1, 1, 1, 3, BaseType> YYYW;
		impl::_swizzle4d<4, 1, 1, 2, 0, BaseType> YYZX;
		impl::_swizzle4d<4, 1, 1, 2, 1, BaseType> YYZY;
		impl::_swizzle4d<4, 1, 1, 2, 2, BaseType> YYZZ;
		impl::_swizzle4d<4, 1, 1, 2, 3, BaseType> YYZW;
		impl::_swizzle4d<4, 1, 1, 3, 0, BaseType> YYWX;
		impl::_swizzle4d<4, 1, 1, 3, 1, BaseType> YYWY;
		impl::_swizzle4d<4, 1, 1, 3, 2, BaseType> YYWZ;
		impl::_swizzle4d<4, 1, 1, 3, 3, BaseType> YYWW;
		impl::_swizzle4d<4, 1, 2, 0, 1, BaseType> YZXY;
		impl::_swizzle4d<4, 1, 2, 0, 2, BaseType> YZXZ;
		impl::_swizzle4d<4, 1, 2, 0, 3, BaseType> YZXW;
		impl::_swizzle4d<4, 1, 2, 1, 0, BaseType> YZYX;
		impl::_swizzle4d<4, 1, 2, 1, 1, BaseType> YZYY;
		impl::_swizzle4d<4, 1, 2, 1, 2, BaseType> YZYZ;
		impl::_swizzle4d<4, 1, 2, 1, 3, BaseType> YZYW;
		impl::_swizzle4d<4, 1, 2, 2, 0, BaseType> YZZX;
		impl::_swizzle4d<4, 1, 2, 2, 1, BaseType> YZZY;
		impl::_swizzle4d<4, 1, 2, 2, 2, BaseType> YZZZ;
		impl::_swizzle4d<4, 1, 2, 2, 3, BaseType> YZZW;
		impl::_swizzle4d<4, 1, 2, 3, 0, BaseType> YZWX;
		impl::_swizzle4d<4, 1, 2, 3, 1, BaseType> YZWY;
		impl::_swizzle4d<4, 1, 2, 3, 2, BaseType> YZWZ;
		impl::_swizzle4d<4, 1, 2, 3, 3, BaseType> YZWW;
		impl::_swizzle4d<4, 1, 3, 0, 1, BaseType> YWXY;
		impl::_swizzle4d<4, 1, 3, 0, 2, BaseType> YWXZ;
		impl::_swizzle4d<4, 1, 3, 0, 3, BaseType> YWXW;
		impl::_swizzle4d<4, 1, 3, 1, 0, BaseType> YWYX;
		impl::_swizzle4d<4, 1, 3, 1, 1, BaseType> YWYY;
		impl::_swizzle4d<4, 1, 3, 1, 2, BaseType> YWYZ;
		impl::_swizzle4d<4, 1, 3, 1, 3, BaseType> YWYW;
		impl::_swizzle4d<4, 1, 3, 2, 0, BaseType> YWZX;
		impl::_swizzle4d<4, 1, 3, 2, 1, BaseType> YWZY;
		impl::_swizzle4d<4, 1, 3, 2, 2, BaseType> YWZZ;
		impl::_swizzle4d<4, 1, 3, 2, 3, BaseType> YWZW;
		impl::_swizzle4d<4, 1, 3, 3, 0, BaseType> YWWX;
		impl::_swizzle4d<4, 1, 3, 3, 1, BaseType> YWWY;
		impl::_swizzle4d<4, 1, 3, 3, 2, BaseType> YWWZ;
		impl::_swizzle4d<4, 1, 3, 3, 3, BaseType> YWWW;
		impl::_swizzle4d<4, 2, 0, 0, 1, BaseType> ZXXY;
		impl::_swizzle4d<4, 2, 0, 0, 2, BaseType> ZXXZ;
		impl::_swizzle4d<4, 2, 0, 0, 3, BaseType> ZXXW;
		impl::_swizzle4d<4, 2, 0, 1, 0, BaseType> ZXYX;
		impl::_swizzle4d<4, 2, 0, 1, 1, BaseType> ZXYY;
		impl::_swizzle4d<4, 2, 0, 1, 2, BaseType> ZXYZ;
		impl::_swizzle4d<4, 2, 0, 1, 3, BaseType> ZXYW;
		impl::_swizzle4d<4, 2, 0, 2, 0, BaseType> ZXZX;
		impl::_swizzle4d<4, 2, 0, 2, 1, BaseType> ZXZY;
		impl::_swizzle4d<4, 2, 0, 2, 2, BaseType> ZXZZ;
		impl::_swizzle4d<4, 2, 0, 2, 3, BaseType> ZXZW;
		impl::_swizzle4d<4, 2, 0, 3, 0, BaseType> ZXWX;
		impl::_swizzle4d<4, 2, 0, 3, 1, BaseType> ZXWY;
		impl::_swizzle4d<4, 2, 0, 3, 2, BaseType> ZXWZ;
		impl::_swizzle4d<4, 2, 0, 3, 3, BaseType> ZXWW;
		impl::_swizzle4d<4, 2, 1, 0, 1, BaseType> ZYXY;
		impl::_swizzle4d<4, 2, 1, 0, 2, BaseType> ZYXZ;
		impl::_swizzle4d<4, 2, 1, 0, 3, BaseType> ZYXW;
		impl::_swizzle4d<4, 2, 1, 1, 0, BaseType> ZYYX;
		impl::_swizzle4d<4, 2, 1, 1, 1, BaseType> ZYYY;
		impl::_swizzle4d<4, 2, 1, 1, 2, BaseType> ZYYZ;
		impl::_swizzle4d<4, 2, 1, 1, 3, BaseType> ZYYW;
		impl::_swizzle4d<4, 2, 1, 2, 0, BaseType> ZYZX;
		impl::_swizzle4d<4, 2, 1, 2, 1, BaseType> ZYZY;
		impl::_swizzle4d<4, 2, 1, 2, 2, BaseType> ZYZZ;
		impl::_swizzle4d<4, 2, 1, 2, 3, BaseType> ZYZW;
		impl::_swizzle4d<4, 2, 1, 3, 0, BaseType> ZYWX;
		impl::_swizzle4d<4, 2, 1, 3, 1, BaseType> ZYWY;
		impl::_swizzle4d<4, 2, 1, 3, 2, BaseType> ZYWZ;
		impl::_swizzle4d<4, 2, 1, 3, 3, BaseType> ZYWW;
		impl::_swizzle4d<4, 2, 2, 0, 1, BaseType> ZZXY;
		impl::_swizzle4d<4, 2, 2, 0, 2, BaseType> ZZXZ;
		impl::_swizzle4d<4, 2, 2, 0, 3, BaseType> ZZXW;
		impl::_swizzle4d<4, 2, 2, 1, 0, BaseType> ZZYX;
		impl::_swizzle4d<4, 2, 2, 1, 1, BaseType> ZZYY;
		impl::_swizzle4d<4, 2, 2, 1, 2, BaseType> ZZYZ;
		impl::_swizzle4d<4, 2, 2, 1, 3, BaseType> ZZYW;
		impl::_swizzle4d<4, 2, 2, 2, 0, BaseType> ZZZX;
		impl::_swizzle4d<4, 2, 2, 2, 1, BaseType> ZZZY;
		impl::_swizzle4d<4, 2, 2, 2, 2, BaseType> ZZZZ;
		impl::_swizzle4d<4, 2, 2, 2, 3, BaseType> ZZZW;
		impl::_swizzle4d<4, 2, 2, 3, 0, BaseType> ZZWX;
		impl::_swizzle4d<4, 2, 2, 3, 1, BaseType> ZZWY;
		impl::_swizzle4d<4, 2, 2, 3, 2, BaseType> ZZWZ;
		impl::_swizzle4d<4, 2, 2, 3, 3, BaseType> ZZWW;
		impl::_swizzle4d<4, 2, 3, 0, 1, BaseType> ZWXY;
		impl::_swizzle4d<4, 2, 3, 0, 2, BaseType> ZWXZ;
		impl::_swizzle4d<4, 2, 3, 0, 3, BaseType> ZWXW;
		impl::_swizzle4d<4, 2, 3, 1, 0, BaseType> ZWYX;
		impl::_swizzle4d<4, 2, 3, 1, 1, BaseType> ZWYY;
		impl::_swizzle4d<4, 2, 3, 1, 2, BaseType> ZWYZ;
		impl::_swizzle4d<4, 2, 3, 1, 3, BaseType> ZWYW;
		impl::_swizzle4d<4, 2, 3, 2, 0, BaseType> ZWZX;
		impl::_swizzle4d<4, 2, 3, 2, 1, BaseType> ZWZY;
		impl::_swizzle4d<4, 2, 3, 2, 2, BaseType> ZWZZ;
		impl::_swizzle4d<4, 2, 3, 2, 3, BaseType> ZWZW;
		impl::_swizzle4d<4, 2, 3, 3, 0, BaseType> ZWWX;
		impl::_swizzle4d<4, 2, 3, 3, 1, BaseType> ZWWY;
		impl::_swizzle4d<4, 2, 3, 3, 2, BaseType> ZWWZ;
		impl::_swizzle4d<4, 2, 3, 3, 3, BaseType> ZWWW;
		impl::_swizzle4d<4, 3, 0, 0, 1, BaseType> WXXY;
		impl::_swizzle4d<4, 3, 0, 0, 2, BaseType> WXXZ;
		impl::_swizzle4d<4, 3, 0, 0, 3, BaseType> WXXW;
		impl::_swizzle4d<4, 3, 0, 1, 0, BaseType> WXYX;
		impl::_swizzle4d<4, 3, 0, 1, 1, BaseType> WXYY;
		impl::_swizzle4d<4, 3, 0, 1, 2, BaseType> WXYZ;
		impl::_swizzle4d<4, 3, 0, 1, 3, BaseType> WXYW;
		impl::_swizzle4d<4, 3, 0, 2, 0, BaseType> WXZX;
		impl::_swizzle4d<4, 3, 0, 2, 1, BaseType> WXZY;
		impl::_swizzle4d<4, 3, 0, 2, 2, BaseType> WXZZ;
		impl::_swizzle4d<4, 3, 0, 2, 3, BaseType> WXZW;
		impl::_swizzle4d<4, 3, 0, 3, 0, BaseType> WXWX;
		impl::_swizzle4d<4, 3, 0, 3, 1, BaseType> WXWY;
		impl::_swizzle4d<4, 3, 0, 3, 2, BaseType> WXWZ;
		impl::_swizzle4d<4, 3, 0, 3, 3, BaseType> WXWW;
		impl::_swizzle4d<4, 3, 1, 0, 1, BaseType> WYXY;
		impl::_swizzle4d<4, 3, 1, 0, 2, BaseType> WYXZ;
		impl::_swizzle4d<4, 3, 1, 0, 3, BaseType> WYXW;
		impl::_swizzle4d<4, 3, 1, 1, 0, BaseType> WYYX;
		impl::_swizzle4d<4, 3, 1, 1, 1, BaseType> WYYY;
		impl::_swizzle4d<4, 3, 1, 1, 2, BaseType> WYYZ;
		impl::_swizzle4d<4, 3, 1, 1, 3, BaseType> WYYW;
		impl::_swizzle4d<4, 3, 1, 2, 0, BaseType> WYZX;
		impl::_swizzle4d<4, 3, 1, 2, 1, BaseType> WYZY;
		impl::_swizzle4d<4, 3, 1, 2, 2, BaseType> WYZZ;
		impl::_swizzle4d<4, 3, 1, 2, 3, BaseType> WYZW;
		impl::_swizzle4d<4, 3, 1, 3, 0, BaseType> WYWX;
		impl::_swizzle4d<4, 3, 1, 3, 1, BaseType> WYWY;
		impl::_swizzle4d<4, 3, 1, 3, 2, BaseType> WYWZ;
		impl::_swizzle4d<4, 3, 1, 3, 3, BaseType> WYWW;
		impl::_swizzle4d<4, 3, 2, 0, 1, BaseType> WZXY;
		impl::_swizzle4d<4, 3, 2, 0, 2, BaseType> WZXZ;
		impl::_swizzle4d<4, 3, 2, 0, 3, BaseType> WZXW;
		impl::_swizzle4d<4, 3, 2, 1, 0, BaseType> WZYX;
		impl::_swizzle4d<4, 3, 2, 1, 1, BaseType> WZYY;
		impl::_swizzle4d<4, 3, 2, 1, 2, BaseType> WZYZ;
		impl::_swizzle4d<4, 3, 2, 1, 3, BaseType> WZYW;
		impl::_swizzle4d<4, 3, 2, 2, 0, BaseType> WZZX;
		impl::_swizzle4d<4, 3, 2, 2, 1, BaseType> WZZY;
		impl::_swizzle4d<4, 3, 2, 2, 2, BaseType> WZZZ;
		impl::_swizzle4d<4, 3, 2, 2, 3, BaseType> WZZW;
		impl::_swizzle4d<4, 3, 2, 3, 0, BaseType> WZWX;
		impl::_swizzle4d<4, 3, 2, 3, 1, BaseType> WZWY;
		impl::_swizzle4d<4, 3, 2, 3, 2, BaseType> WZWZ;
		impl::_swizzle4d<4, 3, 2, 3, 3, BaseType> WZWW;
		impl::_swizzle4d<4, 3, 3, 0, 1, BaseType> WWXY;
		impl::_swizzle4d<4, 3, 3, 0, 2, BaseType> WWXZ;
		impl::_swizzle4d<4, 3, 3, 0, 3, BaseType> WWXW;
		impl::_swizzle4d<4, 3, 3, 1, 0, BaseType> WWYX;
		impl::_swizzle4d<4, 3, 3, 1, 1, BaseType> WWYY;
		impl::_swizzle4d<4, 3, 3, 1, 2, BaseType> WWYZ;
		impl::_swizzle4d<4, 3, 3, 1, 3, BaseType> WWYW;
		impl::_swizzle4d<4, 3, 3, 2, 0, BaseType> WWZX;
		impl::_swizzle4d<4, 3, 3, 2, 1, BaseType> WWZY;
		impl::_swizzle4d<4, 3, 3, 2, 2, BaseType> WWZZ;
		impl::_swizzle4d<4, 3, 3, 2, 3, BaseType> WWZW;
		impl::_swizzle4d<4, 3, 3, 3, 0, BaseType> WWWX;
		impl::_swizzle4d<4, 3, 3, 3, 1, BaseType> WWWY;
		impl::_swizzle4d<4, 3, 3, 3, 2, BaseType> WWWZ;
		impl::_swizzle4d<4, 3, 3, 3, 3, BaseType> WWWW;

		impl::_swizzle3d<4, 0, 0, 1, BaseType> XXX;
		impl::_swizzle3d<4, 0, 0, 1, BaseType> XXY;
		impl::_swizzle3d<4, 0, 0, 2, BaseType> XXZ;
		impl::_swizzle3d<4, 0, 0, 3, BaseType> XXW;
		impl::_swizzle3d<4, 0, 1, 0, BaseType> XYX;
		impl::_swizzle3d<4, 0, 1, 1, BaseType> XYY;
		impl::_swizzle3d<4, 0, 1, 2, BaseType> XYZ;
		impl::_swizzle3d<4, 0, 1, 3, BaseType> XYW;
		impl::_swizzle3d<4, 0, 2, 0, BaseType> XZX;
		impl::_swizzle3d<4, 0, 2, 1, BaseType> XZY;
		impl::_swizzle3d<4, 0, 2, 2, BaseType> XZZ;
		impl::_swizzle3d<4, 0, 2, 3, BaseType> XZW;
		impl::_swizzle3d<4, 0, 3, 0, BaseType> XWX;
		impl::_swizzle3d<4, 0, 3, 1, BaseType> XWY;
		impl::_swizzle3d<4, 0, 3, 2, BaseType> XWZ;
		impl::_swizzle3d<4, 0, 3, 3, BaseType> XWW;		
		impl::_swizzle3d<4, 1, 0, 0, BaseType> YXX;
		impl::_swizzle3d<4, 1, 0, 1, BaseType> YXY;
		impl::_swizzle3d<4, 1, 0, 2, BaseType> YXZ;
		impl::_swizzle3d<4, 1, 0, 3, BaseType> YXW;
		impl::_swizzle3d<4, 1, 1, 0, BaseType> YYX;
		impl::_swizzle3d<4, 1, 1, 1, BaseType> YYY;
		impl::_swizzle3d<4, 1, 1, 2, BaseType> YYZ;
		impl::_swizzle3d<4, 1, 1, 3, BaseType> YYW;
		impl::_swizzle3d<4, 1, 2, 0, BaseType> YZX;
		impl::_swizzle3d<4, 1, 2, 1, BaseType> YZY;
		impl::_swizzle3d<4, 1, 2, 2, BaseType> YZZ;
		impl::_swizzle3d<4, 1, 2, 3, BaseType> YZW;
		impl::_swizzle3d<4, 1, 3, 0, BaseType> YWX;
		impl::_swizzle3d<4, 1, 3, 1, BaseType> YWY;
		impl::_swizzle3d<4, 1, 3, 2, BaseType> YWZ;
		impl::_swizzle3d<4, 1, 3, 3, BaseType> YWW;		
		impl::_swizzle3d<4, 2, 0, 0, BaseType> ZXX;
		impl::_swizzle3d<4, 2, 0, 1, BaseType> ZXY;
		impl::_swizzle3d<4, 2, 0, 2, BaseType> ZXZ;
		impl::_swizzle3d<4, 2, 0, 3, BaseType> ZXW;
		impl::_swizzle3d<4, 2, 1, 0, BaseType> ZYX;
		impl::_swizzle3d<4, 2, 1, 1, BaseType> ZYY;
		impl::_swizzle3d<4, 2, 1, 2, BaseType> ZYZ;
		impl::_swizzle3d<4, 2, 1, 3, BaseType> ZYW;
		impl::_swizzle3d<4, 2, 2, 0, BaseType> ZZX;
		impl::_swizzle3d<4, 2, 2, 1, BaseType> ZZY;
		impl::_swizzle3d<4, 2, 2, 2, BaseType> ZZZ;
		impl::_swizzle3d<4, 2, 2, 3, BaseType> ZZW;
		impl::_swizzle3d<4, 2, 3, 0, BaseType> ZWX;
		impl::_swizzle3d<4, 2, 3, 1, BaseType> ZWY;
		impl::_swizzle3d<4, 2, 3, 2, BaseType> ZWZ;
		impl::_swizzle3d<4, 2, 3, 3, BaseType> ZWW;		
		impl::_swizzle3d<4, 3, 0, 0, BaseType> WXX;
		impl::_swizzle3d<4, 3, 0, 1, BaseType> WXY;
		impl::_swizzle3d<4, 3, 0, 2, BaseType> WXZ;
		impl::_swizzle3d<4, 3, 0, 3, BaseType> WXW;
		impl::_swizzle3d<4, 3, 1, 0, BaseType> WYX;
		impl::_swizzle3d<4, 3, 1, 1, BaseType> WYY;
		impl::_swizzle3d<4, 3, 1, 2, BaseType> WYZ;
		impl::_swizzle3d<4, 3, 1, 3, BaseType> WYW;
		impl::_swizzle3d<4, 3, 2, 0, BaseType> WZX;
		impl::_swizzle3d<4, 3, 2, 1, BaseType> WZY;
		impl::_swizzle3d<4, 3, 2, 2, BaseType> WZZ;
		impl::_swizzle3d<4, 3, 2, 3, BaseType> WZW;
		impl::_swizzle3d<4, 3, 3, 0, BaseType> WWX;
		impl::_swizzle3d<4, 3, 3, 1, BaseType> WWY;
		impl::_swizzle3d<4, 3, 3, 2, BaseType> WWZ;
		impl::_swizzle3d<4, 3, 3, 3, BaseType> WWW;

		impl::_swizzle2d<4, 0, 0, BaseType> XX;
		impl::_swizzle2d<4, 0, 1, BaseType> XY;
		impl::_swizzle2d<4, 0, 2, BaseType> XZ;
		impl::_swizzle2d<4, 0, 3, BaseType> XW;
		impl::_swizzle2d<4, 1, 0, BaseType> YX;
		impl::_swizzle2d<4, 1, 1, BaseType> YY;
		impl::_swizzle2d<4, 1, 2, BaseType> YZ;
		impl::_swizzle2d<4, 1, 3, BaseType> YW;
		impl::_swizzle2d<4, 2, 0, BaseType> ZX;
		impl::_swizzle2d<4, 2, 1, BaseType> ZY;
		impl::_swizzle2d<4, 2, 2, BaseType> ZZ;
		impl::_swizzle2d<4, 2, 3, BaseType> ZW;
		impl::_swizzle2d<4, 3, 0, BaseType> WX;
		impl::_swizzle2d<4, 3, 1, BaseType> WY;
		impl::_swizzle2d<4, 3, 2, BaseType> WZ;
		impl::_swizzle2d<4, 3, 3, BaseType> WW;
		
		#pragma endregion
	};

	vec() : X(__0), Y(__0), Z(__0), W(__1) {}
	vec(BaseType value) : X(value), Y(value), Z(value), W(value) {}
	vec(BaseType x, BaseType y, BaseType z, BaseType w) : X(x), Y(y), Z(z), W(w) {}
	vec(BaseType x, BaseType y, BaseType z) : X(x), Y(y), Z(z), W(__1) {}
	vec(BaseType x, BaseType y) : X(x), Y(y), Z(__0), W(__1) {}
	template <typename T>
	vec(const vec<2, T>& other) : X(other.X), Y(other.Y), Z(__0), W(__1) { }
	template <typename T>
	vec(const vec<3, T>& other) : X(other.X), Y(other.Y), Z(other.Z), W(__1) { }

	// Binary arithmetic operators	
	inline friend vec<4,BaseType> operator +(const vec<4,BaseType>& l, const vec<4,BaseType>& r)
		{ return vec<4,BaseType>(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W); }
	inline friend vec<4,BaseType> operator -(const vec<4,BaseType>& l, const vec<4,BaseType>& r)
		{ return vec<4,BaseType>(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W); }
	inline friend vec<4,BaseType> operator *(const vec<4,BaseType>& l, const vec<4,BaseType>& r)
		{ return vec<4,BaseType>(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W); }
	inline friend vec<4,BaseType> operator /(const vec<4,BaseType>& l, const vec<4,BaseType>& r)
		{ return vec<4,BaseType>(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W); }
	inline friend vec<4,BaseType> operator +(const vec<4,BaseType>& l, BaseType r)
		{ return vec<4,BaseType>(l.X + r, l.Y + r, l.Z + r, l.W + r); }
	inline friend vec<4,BaseType> operator -(const vec<4,BaseType>& l, BaseType r)
		{ return vec<4,BaseType>(l.X - r, l.Y - r, l.Z - r, l.W - r); }
	inline friend vec<4,BaseType> operator *(const vec<4,BaseType>& l, BaseType r)
		{ return vec<4,BaseType>(l.X * r, l.Y * r, l.Z * r, l.W * r); }
	inline friend vec<4,BaseType> operator /(const vec<4,BaseType>& l, BaseType r)
		{ return vec<4,BaseType>(l.X / r, l.Y / r, l.Z / r, l.W / r); }
	inline friend vec<4,BaseType> operator +(BaseType r, const vec<4,BaseType>& l)
		{ return vec<4,BaseType>(l.X + r, l.Y + r, l.Z + r, l.W + r); }
	inline friend vec<4,BaseType> operator -(BaseType r, const vec<4,BaseType>& l)
		{ return vec<4,BaseType>(l.X - r, l.Y - r, l.Z - r, l.W - r); }
	inline friend vec<4,BaseType> operator *(BaseType r, const vec<4,BaseType>& l)
		{ return vec<4,BaseType>(l.X * r, l.Y * r, l.Z * r, l.W * r); }
	inline friend vec<4,BaseType> operator /(BaseType r, const vec<4,BaseType>& l)
		{ return vec<4,BaseType>(l.X / r, l.Y / r, l.Z / r, l.W / r); }

	// Unary Arithmetic Operators
	
	inline vec<4,BaseType>& operator +=(const vec<4,BaseType>& r) { X += r.X; Y += r.Y; Z += r.Z; W += r.W; return *this; }
	inline vec<4,BaseType>& operator -=(const vec<4,BaseType>& r) { X -= r.X; Y -= r.Y; Z -= r.Z; W -= r.W; return *this; }
	inline vec<4,BaseType>& operator *=(const vec<4,BaseType>& r) { X *= r.X; Y *= r.Y; Z *= r.Z; W *= r.W; return *this; }
	inline vec<4,BaseType>& operator /=(const vec<4,BaseType>& r) { X /= r.X; Y /= r.Y; Z /= r.Z; W /= r.W; return *this; }
	inline vec<4,BaseType>& operator +=(BaseType r) { X += r; Y += r; Z += r; W += r; return *this; }
	inline vec<4,BaseType>& operator -=(BaseType r) { X -= r; Y -= r; Z -= r; W -= r; return *this; }
	inline vec<4,BaseType>& operator *=(BaseType r) { X *= r; Y *= r; Z *= r; W *= r; return *this; }
	inline vec<4,BaseType>& operator /=(BaseType r) { X /= r; Y /= r; Z /= r; W /= r; return *this; }
		
	inline BaseType operator[](size_t i) const { assert(i < 4); return Values[i]; }
	inline BaseType& operator[](size_t i) { assert(i < 4); return Values[i]; }

	vec<4, BaseType> GetNormal() const { return *this * InvSqrt(X * X + Y * Y + Z * Z + W * W); }
	vec<4, BaseType>& Normalize() { *this *= InvSqrt(X * X + Y * Y + Z * Z + W * W); return *this; }
};

// Unary operators
template <typename BaseType>
inline constexpr vec<4,  BaseType> operator +(const vec<4,  BaseType>& v) { return v; }
template <typename BaseType>
inline constexpr vec<4,  BaseType> operator -(const vec<4,  BaseType>& v) { return vec<2, BaseType>(-v.X, -v.Y, -v.Z, -v.W); }
template <typename BaseType>
inline constexpr vec<4,  BaseType> operator ~(const vec<4,  BaseType>& v) { return vec<2, BaseType>(~v.X, ~v.Y, ~v.Z, ~v.W); }

// Equality Operators
template <typename T1, typename T2>
inline constexpr bool operator ==(const vec<4,  T1>& a, const vec<4,  T2>& b) { return a.X == b.X & a.Y == b.Y & a.Z == b.Z & a.W == b.W; }
template <typename T1, typename T2>
inline constexpr bool operator !=(const vec<4,  T1>& a, const vec<4,  T2>& b) { return !(a.X == b.X & a.Y == b.Y & a.Z == b.Z & a.W == b.W); }
template <typename T1, typename T2>
inline constexpr vec<4,  bool> operator &&(const vec<4,  T1>& a, const vec<4,  T2>& b) { return vec<4,  bool>(a.X && b.X, a.Y && b.Y, a.Z && b.Z, a.W && b.W); }
template <typename T1, typename T2>
inline constexpr vec<4,  bool> operator ||(const vec<4,  T1>& a, const vec<4,  T2>& b) { return vec<4,  bool>(a.X || b.X, a.Y || b.Y, a.Z || b.Z, a.W || b.W); }

template <typename BaseType>
inline vec<4,  BaseType> operator +(const vec<4,  BaseType>& l, const vec<4,  BaseType>& r) { return vec<4, BaseType>(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W); }
template <typename BaseType>
inline vec<4,  BaseType> operator -(const vec<4,  BaseType>& l, const vec<4,  BaseType>& r) { return vec<4, BaseType>(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W); }
template <typename BaseType>
inline vec<4,  BaseType> operator *(const vec<4,  BaseType>& l, const vec<4,  BaseType>& r) { return vec<4, BaseType>(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W); }
template <typename BaseType>
inline vec<4,  BaseType> operator /(const vec<4,  BaseType>& l, const vec<4,  BaseType>& r) { return vec<4, BaseType>(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W); }
template <typename BaseType>
inline vec<4, BaseType> operator %(const vec<4, BaseType>& l, const vec<4, BaseType>& r) { return vec<4, BaseType>(Mod(l.X, r.X), Mod(l.Y, r.Y), Mod(l.Z, r.Z), Mod(l.W, r.W)); }

template <typename BaseType>
inline vec<4,  BaseType> operator +(const vec<4,  BaseType>& l, BaseType r) { return vec<4,  BaseType>(l.X + r, l.Y + r, l.Z + r, l.W + r); }
template <typename BaseType>
inline vec<4,  BaseType> operator -(const vec<4,  BaseType>& l, BaseType r) { return vec<4,  BaseType>(l.X - r, l.Y - r, l.Z - r, l.W - r); }
template <typename BaseType>
inline vec<4,  BaseType> operator *(const vec<4,  BaseType>& l, BaseType r) { return vec<4,  BaseType>(l.X * r, l.Y * r, l.Z * r, l.W * r); }
template <typename BaseType>
inline vec<4,  BaseType> operator /(const vec<4,  BaseType>& l, BaseType r) { return vec<4,  BaseType>(l.X / r, l.Y / r, l.Z / r, l.W / r); }
template <typename BaseType>
inline vec<4, BaseType> operator %(const vec<4, BaseType>& l, BaseType r) { return vec<4, BaseType>(Mod(l.X, r), Mod(l.Y, r), Mod(l.Z, r), Mod(l.W, r)); }
template <typename BaseType>
inline vec<4,  BaseType> operator +(BaseType r, const vec<4,  BaseType>& l) { return vec<4, BaseType>(l.X + r, l.Y + r, l.Z + r, l.W + r); }
template <typename BaseType>
inline vec<4,  BaseType> operator -(BaseType r, const vec<4,  BaseType>& l) { return vec<4, BaseType>(l.X - r, l.Y - r, l.Z - r, l.W - r); }
template <typename BaseType>
inline vec<4,  BaseType> operator *(BaseType r, const vec<4,  BaseType>& l) { return vec<4, BaseType>(l.X * r, l.Y * r, l.Z * r, l.W * r); }
template <typename BaseType>
inline vec<4,  BaseType> operator /(BaseType r, const vec<4,  BaseType>& l) { return vec<4, BaseType>(l.X / r, l.Y / r, l.Z / r, l.W / r); }
template <typename BaseType>
inline vec<4, BaseType> operator %(BaseType r, const vec<4, BaseType>& l) { return vec<4, BaseType>(Mod(l.X, r), Mod(l.Y, r), Mod(l.Z, r), Mod(l.W, r)); }

#pragma endregion

template<typename BaseType>
struct mat4_t
{
public:
	typedef BaseType ValueType;
	typedef vec<4, BaseType> ColType;
	typedef vec<4, BaseType> RowType;
	typedef mat4_t<BaseType> Type;
		
private:	
	inline static const BaseType __0  = static_cast<BaseType>(0.0);
	inline static const BaseType __1  = static_cast<BaseType>(1.0);
	inline static const BaseType __N1 = static_cast<BaseType>(-1.0);

	typedef vec<3, BaseType> vec3;
	
	union {
		ColType   _columns[4];
		BaseType  _values[16];
	};
	
public:
	mat4_t(BaseType diagonal) :
		_columns{
			{ diagonal, __0, __0, __0 },
			{ __0, diagonal, __0, __0 },
			{ __0, __0, diagonal, __0 },
			{ __0, __0, __0, diagonal }
		} {}
	mat4_t() : _columns{
		{ __1, __0, __0, __0 },
		{ __0, __1, __0, __0 },
		{ __0, __0, __1, __0 },
		{ __0, __0, __0, __1 }
	} {}
	mat4_t(const ColType& m0, const ColType& m1, const ColType& m2, const ColType& m3) :
	_columns{ m0, m1, m2, m3 } {}
	
	inline ColType& operator [](size_t i) { assert(i < 4); return _columns[i]; }
	inline const ColType& operator [](size_t i) const { assert(i < 4); return _columns[i]; }

	inline friend ColType operator*(const mat4_t& v, const ColType& m) {
		return m[0] * v[0] + m[1] * v[1] + m[2] * v[2] + m[3] * v[3];
	}
	inline friend ColType operator*(const ColType& l, const mat4_t& r) {
		return ColType(
			r[0][0] * l[0] + r[0][1] * l[1] + r[0][2] * l[2] + r[0][3] * l[3],
			r[1][0] * l[0] + r[1][1] * l[1] + r[1][2] * l[2] + r[1][3] * l[3],
			r[2][0] * l[0] + r[2][1] * l[1] + r[2][2] * l[2] + r[2][3] * l[3],
			r[3][0] * l[0] + r[3][1] * l[1] + r[3][2] * l[2] + r[3][3] * l[3]
		);
	}
	inline friend vec3 operator*(const mat4_t& l, const vec3& r) { return (vec3)(l * (ColType)r); }
	inline friend vec3 operator*(const vec3& l, const mat4_t& r) { return (vec3)((ColType)l * r); };
	
	inline friend mat4_t operator*(const mat4_t& l, BaseType r) {
		return mat4_t(l[0] * r, l[1] * r, l[2] * r, l[3] * r);
	}
	inline friend mat4_t operator*(BaseType l, const mat4_t& r) {
		return mat4_t(r[0] * l, r[1] * l, r[2] * l, r[3] * l);
	}
	inline friend mat4_t operator/(const mat4_t& l, BaseType r) {
		return mat4_t(l[0] / r, l[1] / r, l[2] / r, l[3] / r);
	}
	inline friend mat4_t operator/(BaseType l, const mat4_t& r) {
		return mat4_t(r[0] / l, r[1] / l, r[2] / l, r[3] / l);
	}

	inline friend mat4_t operator*(const mat4_t& l, const mat4_t& r) {
		mat4_t<BaseType> result;
		result[0] = l[0] * r[0][0] + l[1] * r[0][1] + l[2] * r[0][2] + l[3] * r[0][3];
		result[1] = l[0] * r[1][0] + l[1] * r[1][1] + l[2] * r[1][2] + l[3] * r[1][3];
		result[2] = l[0] * r[2][0] + l[1] * r[2][1] + l[2] * r[2][2] + l[3] * r[2][3];
		result[3] = l[0] * r[3][0] + l[1] * r[3][1] + l[2] * r[3][2] + l[3] * r[3][3];
		return result;
	}
	inline friend mat4_t operator/(const mat4_t& l, const mat4_t& r) {
		mat4_t result = l;
		return result /= r;
	}
	
	inline mat4_t& operator +=(const mat4_t& v) { _columns[0] += v[0]; _columns[1] += v[1]; _columns[2] += v[2]; _columns[3] += v[3]; return *this; }
	inline mat4_t& operator -=(const mat4_t& v) { _columns[0] -= v[0]; _columns[1] -= v[1]; _columns[2] -= v[2]; _columns[3] -= v[3]; return *this; }
	inline mat4_t& operator *=(const mat4_t& v) { return *this = *this * v; }
	inline mat4_t& operator /=(const mat4_t& v) { return *this *= Inverse(v); }
	inline mat4_t& operator +=(const ColType& v) { _columns[0] += v;    _columns[1] += v;    _columns[2] += v;    _columns[3] += v;    return *this; }
	inline mat4_t& operator -=(const ColType& v) { _columns[0] -= v;    _columns[1] -= v;    _columns[2] -= v;    _columns[3] -= v;    return *this; }
	inline mat4_t& operator *=(const ColType& v) { _columns[0] *= v;    _columns[1] *= v;    _columns[2] *= v;    _columns[3] *= v;    return *this; }
	inline mat4_t& operator /=(const ColType& v) { _columns[0] /= v;    _columns[1] /= v;    _columns[2] /= v;    _columns[3] /= v;    return *this; }
	inline mat4_t& operator +=(BaseType v)       { _columns[0] += v;    _columns[1] += v;    _columns[2] += v;    _columns[3] += v;    return *this; }
	inline mat4_t& operator -=(BaseType v)       { _columns[0] -= v;    _columns[1] -= v;    _columns[2] -= v;    _columns[3] -= v;    return *this; }
	inline mat4_t& operator *=(BaseType v)       { _columns[0] *= v;    _columns[1] *= v;    _columns[2] *= v;    _columns[3] *= v;    return *this; }
	inline mat4_t& operator /=(BaseType v)       { _columns[0] /= v;    _columns[1] /= v;    _columns[2] /= v;    _columns[3] /= v;    return *this; }
	
	inline mat4_t& operator =(const mat4_t& other) {
		memcpy(this, &other, sizeof(BaseType) * 16);
		return *this;
	}

	/*
	 * Returns the determinant of a matrix
	 * @param m The matrix to calculate the determinant for
	 * @returns The determinant of matrix m
	 * @see https://en.wikipedia.org/wiki/Determinant#n_%C3%97_n_matrices
	 */
	static inline BaseType Determinant(const mat4_t& m)
	{
		// Note: This implementation is taken directly from the GLM implementation found here:
		// https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
		BaseType SubFactor00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
		BaseType SubFactor01 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
		BaseType SubFactor02 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
		BaseType SubFactor03 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
		BaseType SubFactor04 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
		BaseType SubFactor05 = m[2][0] * m[3][1] - m[3][0] * m[2][1];

		ColType DetCof(
			+(m[1][1] * SubFactor00 - m[1][2] * SubFactor01 + m[1][3] * SubFactor02),
			-(m[1][0] * SubFactor00 - m[1][2] * SubFactor03 + m[1][3] * SubFactor04),
			+(m[1][0] * SubFactor01 - m[1][1] * SubFactor03 + m[1][3] * SubFactor05),
			-(m[1][0] * SubFactor02 - m[1][1] * SubFactor04 + m[1][2] * SubFactor05));

		return
			m[0][0] * DetCof[0] + m[0][1] * DetCof[1] +
			m[0][2] * DetCof[2] + m[0][3] * DetCof[3];
	}	
	/*
	 * Returns the inverse of a matrix
	 * @param m The matrix to calculate the inverse of
	 * @returns the Inverse matrix of m
	 * @see https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_4_%C3%97_4_matrices
	 * @see https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
	 */
	static inline mat4_t Inverse(const mat4_t& m) {
		// Note: This implementation is taken directly from the GLM implementation found here:
		// https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
		BaseType Coef00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
		BaseType Coef02 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
		BaseType Coef03 = m[1][2] * m[2][3] - m[2][2] * m[1][3];

		BaseType Coef04 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
		BaseType Coef06 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
		BaseType Coef07 = m[1][1] * m[2][3] - m[2][1] * m[1][3];

		BaseType Coef08 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
		BaseType Coef10 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
		BaseType Coef11 = m[1][1] * m[2][2] - m[2][1] * m[1][2];

		BaseType Coef12 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
		BaseType Coef14 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
		BaseType Coef15 = m[1][0] * m[2][3] - m[2][0] * m[1][3];

		BaseType Coef16 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
		BaseType Coef18 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
		BaseType Coef19 = m[1][0] * m[2][2] - m[2][0] * m[1][2];

		BaseType Coef20 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
		BaseType Coef22 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
		BaseType Coef23 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

		ColType Fac0(Coef00, Coef00, Coef02, Coef03);
		ColType Fac1(Coef04, Coef04, Coef06, Coef07);
		ColType Fac2(Coef08, Coef08, Coef10, Coef11);
		ColType Fac3(Coef12, Coef12, Coef14, Coef15);
		ColType Fac4(Coef16, Coef16, Coef18, Coef19);
		ColType Fac5(Coef20, Coef20, Coef22, Coef23);

		ColType Vec0(m[1][0], m[0][0], m[0][0], m[0][0]);
		ColType Vec1(m[1][1], m[0][1], m[0][1], m[0][1]);
		ColType Vec2(m[1][2], m[0][2], m[0][2], m[0][2]);
		ColType Vec3(m[1][3], m[0][3], m[0][3], m[0][3]);

		ColType Inv0(Vec1 * Fac0 - Vec2 * Fac1 + Vec3 * Fac2);
		ColType Inv1(Vec0 * Fac0 - Vec2 * Fac3 + Vec3 * Fac4);
		ColType Inv2(Vec0 * Fac1 - Vec1 * Fac3 + Vec3 * Fac5);
		ColType Inv3(Vec0 * Fac2 - Vec1 * Fac4 + Vec2 * Fac5);

		ColType SignA(+1, -1, +1, -1);
		ColType SignB(-1, +1, -1, +1);
		mat4_t Inverse(Inv0 * SignA, Inv1 * SignB, Inv2 * SignA, Inv3 * SignB);

		ColType Row0(Inverse[0][0], Inverse[1][0], Inverse[2][0], Inverse[3][0]);

		ColType Dot0(m[0] * Row0);
		BaseType Dot1 = (Dot0.X + Dot0.Y) + (Dot0.Z + Dot0.W);

		BaseType OneOverDeterminant = 1.0 / Dot1;

		return Inverse * OneOverDeterminant;
	}
	/*
	 * Returns the transpose of a matrix, reflecting it along it's main diagonal
	 * (can be used to convert from row major to column major)
	 * @param m The matrix to calculate the transpose of
	 * @returns The transpose matrix of m
	 * @see https://en.wikipedia.org/wiki/Transpose
	 */
	static inline mat4_t Transpose(const mat4_t& m) {
		// Note: This implementation is taken directly from the GLM implementation found here:
		// https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
		mat4_t result;
		result[0][0] = m[0][0];
		result[0][1] = m[1][0];
		result[0][2] = m[2][0];
		result[0][3] = m[3][0];

		result[1][0] = m[0][1];
		result[1][1] = m[1][1];
		result[1][2] = m[2][1];
		result[1][3] = m[3][1];

		result[2][0] = m[0][2];
		result[2][1] = m[1][2];
		result[2][2] = m[2][2];
		result[2][3] = m[3][2];

		result[3][0] = m[0][3];
		result[3][1] = m[1][3];
		result[3][2] = m[2][3];
		result[3][3] = m[3][3];
		return result;
	}

	static inline mat4_t Translate(const mat4_t& m, const vec<3, BaseType>& offset) {
		mat4_t result = m;
		result[3] = m[0] * offset[0] + m[1] * offset[1] + m[2] * offset[2] + m[3];
		return result;
	}
	static inline mat4_t Translate(const vec<3, BaseType>& offset) {
		mat4_t result;
		result[3] = result[0] * offset[0] + result[1] * offset[1] + result[2] * offset[2] + result[3];
		return result;
	}
	
	static inline mat4_t Scale(const mat4_t& m, const vec<3, BaseType>& scale) {
		mat4_t result;
		result[0] = m[0] * scale[0];
		result[1] = m[1] * scale[1];
		result[2] = m[2] * scale[2];
		result[3] = m[3];
		return result;
	}
	static inline mat4_t Scale(const vec<3, BaseType>& scale) {
		return mat4(
			vec4(scale.X, __0,     __0,     __0),
			vec4(__0    , scale.Y, __0,     __0),
			vec4(__0    , __0,     scale.Z, __0),
			vec4(__0    , __0,     __0,     __1)
		);
	}
	
	static inline mat4_t Rotate(const mat4_t& m, BaseType radians, const vec3& axis) {
		float c = cosf(radians);
		float s = sinf(radians);

		vec3 n = Normalize(axis);
		vec3 t = (__1 - c) * n;

		mat4_t rot;
		rot[0][0] = c + t[0] * n[0];
		rot[0][1] = t[0] * n[1] + s * n[2];
		rot[0][2] = t[0] * n[2] - s * n[1];

		rot[1][0] = t[1] * n[0] - s * n[2];
		rot[1][1] = c + t[1] * n[1];
		rot[1][2] = t[1] * n[2] + s * n[0];

		rot[2][0] = t[2] * n[0] + s * n[1];
		rot[2][1] = t[2] * n[1] - s * n[0];
		rot[2][2] = c + t[2] * n[2];

		mat4_t result;
		result[0] = m[0] * rot[0][0] + m[1] * rot[0][1] + m[2] * rot[0][2];
		result[1] = m[0] * rot[1][0] + m[1] * rot[1][1] + m[2] * rot[1][2];
		result[2] = m[0] * rot[2][0] + m[1] * rot[2][1] + m[2] * rot[2][2];
		result[3] = m[3];		
		return result;
	}
	static inline mat4_t Rotate(BaseType radians, const vec3& axis) {
		float c = cosf(radians);
		float s = sinf(radians);

		vec3 n = Normalize(axis);
		vec3 t = (__1 - c) * n;

		mat4_t rot;
		rot[0][0] = c + t[0] * n[0];
		rot[0][1] = t[0] * n[1] + s * n[2];
		rot[0][2] = t[0] * n[2] - s * n[1];

		rot[1][0] = t[1] * n[0] - s * n[2];
		rot[1][1] = c + t[1] * n[1];
		rot[1][2] = t[1] * n[2] + s * n[0];

		rot[2][0] = t[2] * n[0] - s * n[1];
		rot[2][1] = t[2] * n[1] + s * n[0];
		rot[2][2] = c + t[2] * n[2];

		return rot;
	}
	
	static inline mat4_t LookAtRH(const vec3& eye, const vec3& center, const vec3& up) {
		vec3 f = Normalize(center - eye);
		vec3 s = Normalize(Cross(f, up));
		vec3 u = Cross(s, f);

		mat4_t result;
		result[0][0] = s.X;
		result[1][0] = s.Y;
		result[2][0] = s.Z;
		result[0][1] = u.X;
		result[1][1] = u.Y;
		result[2][1] = u.Z;
		result[0][2] = -f.X;
		result[1][2] = -f.Y;
		result[2][2] = -f.Z;
		result[3][0] = -Dot(s, eye);
		result[3][1] = -Dot(u, eye);
		result[3][2] =  Dot(f, eye);
		return result;
	}
	static inline mat4_t LookAtLH(const vec3& eye, const vec3& center, const vec3& up) {
		vec3 f = Normalize(center - eye);
		vec3 s = Normalize(Cross(up, f));
		vec3 u = Cross(f, s);

		mat4_t result;
		result[0][0] = s.X;
		result[1][0] = s.Y;
		result[2][0] = s.Z;
		result[0][1] = u.X;
		result[1][1] = u.Y;
		result[2][1] = u.Z;
		result[0][2] = f.X;
		result[1][2] = f.Y;
		result[2][2] = f.Z;
		result[3][0] = -Dot(s, eye);
		result[3][1] = -Dot(u, eye);
		result[3][2] = -Dot(f, eye);
		return result;
	}

	static inline mat4_t PerspectiveFovLH(BaseType fovRadians, BaseType width, BaseType height, BaseType zNear, BaseType zFar) {
		assert(width > __0);
		assert(height > __0);
		assert(fovRadians > __0);

		const double rad = fovRadians;
		const BaseType h = Cos(static_cast<BaseType>(0.5 * rad)) / Sin(static_cast<BaseType>(0.5 * rad));
		const BaseType w = h * height / width;
		
		mat4_t result(__0);
		result[0][0] = static_cast<BaseType>(w);
		result[1][1] = static_cast<BaseType>(h);
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = (zFar + zNear) / (zFar - zNear);
		result[2][3] = __1;
		result[3][2] = -(static_cast<BaseType>(2.0) * zFar * zNear) / (zFar - zNear);
		#else
		result[2][2] = far / (far - near);
		result[2][3] = __1;
		result[3][2] = -(far * near) / (far - near);
		#endif
		return result;
	}
	static inline mat4_t PerspectiveFovRH(BaseType fovRadians, BaseType width, BaseType height, BaseType zNear, BaseType zFar) {
		assert(width > __0);
		assert(height > __0);
		assert(fovRadians > __0);

		const double rad = fovRadians;
		const BaseType h = Cos(static_cast<BaseType>(0.5 * rad)) / Sin(static_cast<BaseType>(0.5* rad));
		const BaseType w = h * height / width;

		mat4_t result(__0);
		result[0][0] = w;
		result[1][1] = h;
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = -(zFar + zNear) / (zFar - zNear);
		result[2][3] = -__1;
		result[3][2] = -(static_cast<BaseType>(2.0)* zFar * zNear) / (zFar - zNear);
		#else
		result[2][2] = far / (far - near);
		result[2][3] = __1;
		result[3][2] = -(far * near) / (far - near);
		#endif
		return result;
	}
	static inline mat4_t PerspectiveLH(BaseType fovy, BaseType aspect, BaseType zNear, BaseType zFar) {
		assert(abs(aspect - std::numeric_limits<BaseType>::epsilon()) > __0);
		
		const BaseType tanh = static_cast<BaseType>(tan(fovy / 2.0));
		mat4_t result(__0);
		result[0][0] = __1 / (aspect * tanh);
		result[1][1] = __1 / tanh;
		result[2][3] = __1;
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = (zFar + zNear) / (zFar - zNear);
		result[3][2] = -(static_cast<BaseType>(2.0) * zFar * zNear) / (zFar - zNear);
		#else
		result[2][2] = far / (far - near);
		result[3][2] = -(far * near) / (far - near);
		#endif
		return result;
	}
	static inline mat4_t PerspectiveRH(BaseType fovy, BaseType aspect, BaseType zNear, BaseType zFar) {
		assert(abs(aspect - std::numeric_limits<BaseType>::epsilon()) > __0);

		const BaseType tanh = static_cast<BaseType>(tan(fovy / 2.0));
		mat4_t result(__0);
		result[0][0] = __1 / (aspect * tanh);
		result[1][1] = __1 / tanh;
		result[2][3] = -__1;
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = - (zFar + zNear) / (zFar - zNear);
		result[3][2] = -(static_cast<BaseType>(2.0)* zFar * zNear) / (zFar - zNear);
		#else
		result[2][2] = far / (far - near);
		result[3][2] = -(far * near) / (far - near);
		#endif
		return result;
	}
	static inline mat4_t OrthoLH(BaseType left, BaseType right, BaseType bottom, BaseType top, BaseType zNear, BaseType zFar) {
		mat4_t result;
		result[0][0] = static_cast<BaseType>(2.0) / (right - left);
		result[1][1] = static_cast<BaseType>(2.0) / (top - bottom);
		result[2][2] = static_cast<BaseType>(2.0) / (zFar - zNear);
		result[3][0] = -(right + left) / (right - left);
		result[3][1] = -(top + bottom) / (top - bottom);
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = -(zFar + zNear) / (zFar - zNear);
		#else
		result[2][2] = -near / (far - near);
		#endif
		return result;
	}
	static inline mat4_t OrthoRH(BaseType left, BaseType right, BaseType bottom, BaseType top, BaseType zNear, BaseType zFar) {
		mat4_t result;
		result[0][0] = static_cast<BaseType>(2.0) / (right - left);
		result[1][1] = static_cast<BaseType>(2.0) / (top - bottom);
		result[2][2] = -static_cast<BaseType>(2.0) / (zFar - zNear);
		result[3][0] = -(right + left) / (right - left);
		result[3][1] = -(top + bottom) / (top - bottom);
		#if MATH_CLIP_MODE == CLIP_NEGATIVE_TO_ONE
		result[2][2] = -(zFar + zNear) / (zFar - zNear);
		#else
		result[2][2] = -near / (far - near);
		#endif
		return result;
	}
};

template<typename BaseType>
struct mat3_t
{
public:
	typedef BaseType _ValueType;
	typedef mat3_t<BaseType> _Type;
	typedef vec<3, BaseType> ColType;
	typedef vec<3, BaseType> RowType;

private:
	inline static const BaseType __0 = static_cast<BaseType>(0.0);
	inline static const BaseType __1 = static_cast<BaseType>(1.0);

	typedef vec<2, BaseType> vec2;

	union {
		ColType   _columns[3];
		BaseType  _values[9];
	};
	
public:
	mat3_t(BaseType diagonal) :
		_columns{
			{ diagonal, __0, __0},
			{ __0, diagonal, __0},
			{ __0, __0, diagonal}
		} {}
	mat3_t() : _columns{
		{ __1, __0, __0},
		{ __0, __1, __0},
		{ __0, __0, __1}
	} {}
	mat3_t(const ColType& m0, const ColType& m1, const ColType& m2) :
		_columns{ m0, m1, m2 } {}

	inline ColType& operator [](size_t i) { assert(i < 3); return _columns[i]; }
	inline const ColType& operator [](size_t i) const { assert(i < 3); return _columns[i]; }

	inline friend ColType operator*(const mat3_t& v, const ColType& m) {
		return m[0] * v[0] + m[1] * v[1] + m[2] * v[2];
	}
	inline friend ColType operator*(const ColType& l, const mat3_t& r) {
		return ColType(
			r[0][0] * l[0] + r[0][1] * l[1] + r[0][2] * l[2],
			r[1][0] * l[0] + r[1][1] * l[1] + r[1][2] * l[2],
			r[2][0] * l[0] + r[2][1] * l[1] + r[2][2] * l[2]
		);
	}

	inline friend vec2 operator*(const mat3_t& l, const vec2& r) { return (vec2)(l * (ColType)r); }
	inline friend vec2 operator*(const vec2& l, const mat3_t& r) { return (vec2)((ColType)l * r); };

	inline friend mat3_t operator*(const mat3_t& l, BaseType r) {
		return mat3_t(l[0] * r, l[1] * r, l[2] * r);
	}
	inline friend mat3_t operator*(BaseType l, const mat3_t& r) {
		return mat3_t(r[0] * l, r[1] * l, r[2] * l);
	}
	inline friend mat3_t operator/(const mat3_t& l, BaseType r) {
		return mat3_t(l[0] / r, l[1] / r, l[2] / r);
	}
	inline friend mat3_t operator/(BaseType l, const mat3_t& r) {
		return mat3_t(r[0] / l, r[1] / l, r[2] / l);
	}

	inline friend mat3_t operator*(const mat3_t& l, const mat3_t& r) {
		mat3_t result;
		result[0] = l[0] * r[0][0] + l[1] * r[0][1] + l[2] * r[0][2];
		result[1] = l[1] * r[1][0] + l[1] * r[1][1] + l[2] * r[1][2];
		result[2] = l[2] * r[2][0] + l[2] * r[2][1] + l[2] * r[2][2];
		return result;
	}
	inline friend mat3_t operator/(const mat3_t& l, const mat3_t& r) {
		mat3_t result = l;
		return result /= r;
	}

	inline mat3_t& operator +=(const mat3_t& v) { _columns[0] += v[0]; _columns[1] += v[1]; _columns[2] += v[2]; return *this; }
	inline mat3_t& operator -=(const mat3_t& v) { _columns[0] -= v[0]; _columns[1] -= v[1]; _columns[2] -= v[2]; return *this; }
	inline mat3_t& operator *=(const mat3_t& v) { return *this = *this * v; }
	inline mat3_t& operator /=(const mat3_t& v) { return *this *= Inverse(v); }
	inline mat3_t& operator +=(const ColType& v) { _columns[0] += v;    _columns[1] += v;    _columns[2] += v;    return *this; }
	inline mat3_t& operator -=(const ColType& v) { _columns[0] -= v;    _columns[1] -= v;    _columns[2] -= v;    return *this; }
	inline mat3_t& operator *=(const ColType& v) { _columns[0] *= v;    _columns[1] *= v;    _columns[2] *= v;    return *this; }
	inline mat3_t& operator /=(const ColType& v) { _columns[0] /= v;    _columns[1] /= v;    _columns[2] /= v;    return *this; }
	inline mat3_t& operator +=(BaseType v) { _columns[0] += v;    _columns[1] += v;    _columns[2] += v;    return *this; }
	inline mat3_t& operator -=(BaseType v) { _columns[0] -= v;    _columns[1] -= v;    _columns[2] -= v;    return *this; }
	inline mat3_t& operator *=(BaseType v) { _columns[0] *= v;    _columns[1] *= v;    _columns[2] *= v;    return *this; }
	inline mat3_t& operator /=(BaseType v) { _columns[0] /= v;    _columns[1] /= v;    _columns[2] /= v;    return *this; }

	inline mat3_t& operator =(const mat3_t& other) {
		memcpy(this, &other, sizeof(BaseType) * 9);
		return *this;
	}

	/*
	 * Returns the determinant of a matrix
	 * @param m The matrix to calculate the determinant for
	 * @returns The determinant of matrix m
	 * @see https://en.wikipedia.org/wiki/Determinant#n_%C3%97_n_matrices
	 */
	static inline BaseType Determinant(const mat3_t& m)
	{
		return
			+ m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2])
			- m[1][0] * (m[0][1] * m[2][2] - m[2][1] * m[0][2])
			+ m[2][0] * (m[0][1] * m[1][2] - m[1][1] * m[0][2]);
	}
	/*
	 * Returns the inverse of a matrix
	 * @param m The matrix to calculate the inverse of
	 * @returns the Inverse matrix of m
	 * @see https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_3_%C3%97_3_matrices
	 * @see https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
	 */
	static inline mat3_t Inverse(const mat3_t& m) {

		double oneOverD = 1.0 / mat3_t::Determinant(m);

		mat3_t result;
		result[0][0] = static_cast<BaseType>(+(m[1][1] * m[2][2] - m[2][1] * m[1][2]) * oneOverD);
		result[1][0] = static_cast<BaseType>(-(m[1][0] * m[2][2] - m[2][0] * m[1][2]) * oneOverD);
		result[1][0] = static_cast<BaseType>(+(m[1][0] * m[2][1] - m[2][0] * m[1][1]) * oneOverD);
		result[1][0] = static_cast<BaseType>(-(m[0][1] * m[2][2] - m[2][1] * m[0][2]) * oneOverD);
		result[1][1] = static_cast<BaseType>(+(m[0][0] * m[2][2] - m[2][0] * m[0][2]) * oneOverD);
		result[1][2] = static_cast<BaseType>(-(m[0][0] * m[2][1] - m[2][0] * m[0][1]) * oneOverD);
		result[2][0] = static_cast<BaseType>(+(m[0][1] * m[1][2] - m[1][1] * m[0][2]) * oneOverD);
		result[2][1] = static_cast<BaseType>(-(m[0][0] * m[1][2] - m[1][0] * m[0][2]) * oneOverD);
		result[2][2] = static_cast<BaseType>(+(m[0][0] * m[1][1] - m[1][0] * m[0][1]) * oneOverD);

		return result;
	}
	/*
	 * Returns the transpose of a matrix, reflecting it along it's main diagonal
	 * (can be used to convert from row major to column major)
	 * @param m The matrix to calculate the transpose of
	 * @returns The transpose matrix of m
	 * @see https://en.wikipedia.org/wiki/Transpose
	 */
	static inline mat3_t Transpose(const mat3_t& m) {
		// Note: This implementation is taken directly from the GLM implementation found here:
		// https://github.com/g-truc/glm/blob/master/glm/detail/func_matrix.inl
		mat3_t result;
		result[0][0] = m[0][0];
		result[0][1] = m[1][0];
		result[0][2] = m[2][0];

		result[1][0] = m[0][1];
		result[1][1] = m[1][1];
		result[1][2] = m[2][1];

		result[2][0] = m[0][2];
		result[2][1] = m[1][2];
		result[2][2] = m[2][2];
		
		return result;
	}

	static inline mat3_t Translate(const mat3_t& m, const vec2& offset) {
		mat3_t result = m;
		result[2] = m[0] * offset[0] + m[1] * offset[1] + m[2];
		return result;
	}
	static inline mat3_t Translate(const vec2& offset) {
		mat3_t result;
		result[2] = result[0] * offset[0] + result[1] * offset[1] + result[2];
		return result;
	}

	static inline mat3_t Scale(const mat3_t& m, const vec2& scale) {
		mat3_t result;
		result[0] = m[0] * scale[0];
		result[1] = m[1] * scale[1];
		result[2] = m[2];
		return result;
	}
	static inline mat3_t Scale(const vec2& scale) {
		return mat3_t(
			ColType(scale.X, __0, __0),
			ColType(__0, scale.Y, __0),
			ColType(__0, __0, __1)
		);
	}

	static inline mat3_t Rotate(const mat3_t& m, BaseType radians) {
		BaseType c = static_cast<BaseType>(cos(static_cast<double>(radians)));
		BaseType s = static_cast<BaseType>(sin(static_cast<double>(radians)));
		
		mat3_t result;
		result[0] = m[0] * c + m[1] * s;
		result[1] = m[0] * -s + m[1] * c;
		result[2] = m[2];
		return result;
	}
	static inline mat3_t Rotate(BaseType radians, const ColType& axis) {
		BaseType c = static_cast<BaseType>(cos(static_cast<double>(radians)));
		BaseType s = static_cast<BaseType>(sin(static_cast<double>(radians)));

		mat3_t result;
		result[0] = ColType(1,0,0) * c + ColType(0,1,0) * s;
		result[1] = ColType(1, 0, 0) * -s + ColType(0, 1, 0) * c;
		result[2] = ColType(0, 0, 1);
		return result;
	}

	static inline mat3_t ShearX(const mat3_t& m, BaseType y) {
		mat3_t result;
		result[0][1] = y;
		return m * result;
	}
	static inline mat3_t ShearY(const mat3_t& m, BaseType x) {
		mat3_t result;
		result[1][0] = x;
		return m * result;
	}

};

#pragma region Functions

namespace impl
{
	template<class T, class EqualTo>
	struct HasMod_impl {
		template<class U, class V>
		static auto test(U*) -> decltype(std::declval<U>() % std::declval<V>());
		template<typename, typename>
		static auto test(...)->std::false_type;

		using type = typename std::is_same<bool, decltype(test<T, EqualTo>(nullptr))>::type;
	};

	template <typename T, class ModTo = T>
	struct HasMod : HasMod_impl<T, ModTo>::type {};
}

template<typename ValueType>
constexpr  ValueType Clamp(const ValueType& x, const ValueType& min, const ValueType& max) {
	return x < min ?
		min :
		(x > max ?
			max :
			x);
}
template<typename ValueType>
constexpr ValueType Lerp(const ValueType& a, const ValueType& b, float t) {
	return (1 - t) * a + t * b;
}
template<typename ValueType>
constexpr ValueType FastLerp(const ValueType& a, const ValueType& b, float t) {
	return a + t * (b - a);
}
template<typename ValueType>
constexpr ValueType SmoothStep(const ValueType& a, const ValueType& b, const ValueType& t) {
	t = Clamp((t - a) / (b - a), static_cast<ValueType>(0), static_cast<ValueType>(1));
	return t * t * (static_cast<ValueType>(3) - static_cast<ValueType>(2)* t);
}

template <typename T1, typename T2, typename = void>
inline T1 Mod(T1 x, T2 y) {
	return x % y;
}
template <>
inline float Mod(float x, float y) {
	if (x < 0) { return -std::fmodf(x * -1, y) + y; }
	else { return std::fmodf(x, y); }
}
template <>
inline double Mod(double x, double y) {
	if (x < 0) { return -std::fmod(x * -1, y) + y; }
	else { return std::fmod(x, y); }
}

template <typename ValueType>
constexpr ValueType Degrees(ValueType radians) {
	static const double c = 180.0 / Pi<float>();
	return static_cast<ValueType>(radians * c);
}
template <typename ValueType>
constexpr ValueType Radians(ValueType degrees) {
	static const double c = Pi<double>() / 180.0;
	return static_cast<ValueType>(degrees * c);
}

template <typename ValueType>
ValueType Sin(ValueType x) {
	return static_cast<ValueType>(sin(static_cast<double>(x)));
}
template <>
inline float Sin(float x) {
	return std::sinf(x);
}
template <>
inline double Sin(double x) {
	return std::sin(x);
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Sin(const vec<Elements, T>& l) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Sin(l[ix]);
	return result;
}

template <typename ValueType>
ValueType Cos(ValueType x) {
	return static_cast<ValueType>(cos(static_cast<double>(x)));
}
template <>
inline float Cos(float x) {
	return cosf(x);
}
template <>
inline double Cos(double x) {
	return cos(x);
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Cos(const vec<Elements, T>& l) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Cos(l[ix]);
	return result;
}

template <typename ValueType>
ValueType Tan(ValueType x) {
	return static_cast<ValueType>(tan(static_cast<double>(x)));
}
template <>
inline float Tan(float x) {
	return tanf(x);
}
template <>
inline double Tan(double x) {
	return tan(x);
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Tan(const vec<Elements, T>& l) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Tan(l[ix]);
	return result;
}

template <typename ValueType>
ValueType Sqrt(ValueType x) {
	return static_cast<ValueType>(sqrt(static_cast<double>(x)));
}
template <>
inline double Sqrt(double x) {
	return sqrt(x);
}
template <>
inline float Sqrt(float x) {
	return sqrtf(x);
}

template <typename ValueType>
inline constexpr ValueType InvSqrt(ValueType x) {
	return static_cast<double>(1) / std::sqrt(static_cast<double>(x));
}

/*
 * Computes the inverse square root of x, using the fast inverse square root function made
 * popular by Quake
 * @see https://en.m.wikipedia.org/wiki/Fast_inverse_square_root
 * @param x The value to calculate the inverse square root of
 * @return 1 / sqrt(x)
 */
template <>
inline constexpr float InvSqrt(float x) {
	const uint32_t magic = 0x5F3759DF;
	const float x2 = x * 0.5f;
	union {
		float    f;
		uint32_t i;
	} interop = { x };
	interop.i = magic - (interop.i >> 1);
	interop.f *= (1.5f - (x2 * interop.f * interop.f));
	return interop.f;
}
template <>
inline constexpr double InvSqrt(double x) {
	const uint64_t magic = 0x5FE6EB50C7B537A9;
	const double x2 = x * 0.5f;
	union {
		double   f;
		uint64_t i;
	} interop = { x };
	interop.i = magic - (interop.i >> 1);
	interop.f *= (1.5f - (x2 * interop.f * interop.f));
	return interop.f;
}

template <typename T>
static inline T Wrap(const T& value, const T& min, const T& max) {
	const T range = max - min;
	const T start = value - min;
	const T mod = Mod(start, range);
	return min + mod;
}

template <typename T>
static inline T Min(const T& l, const T& r) {
	return l < r ? l : r;
}
template <typename T>
static inline T Max(const T& l, const T& r) {
	return l > r ? l : r;
}

template <size_t Elements, typename T>
static inline vec<Elements, T> Min(const vec<Elements, T>& l, const vec<Elements, T>& r) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Min(l[ix], r[ix]);
	return result;
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Max(const vec<Elements, T>& l, const vec<Elements, T>& r) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Max(l[ix], r[ix]);
	return result;
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Clamp(const vec<Elements, T>& v, const vec<Elements, T>& min, const vec<Elements, T>& max) {
	vec<Elements, T> result;
	for (int ix = 0; ix < Elements; ix++)
		result[ix] = Clamp(v[ix], min[ix], max[ix]);
	return result;
}

#pragma endregion

#pragma region Vector Functions

template <size_t Elements, typename T>
constexpr T Dot(const vec<Elements, T>& a, const vec<Elements, T>& b) {
	T result = static_cast<T>(0.0);
	for (int i = 0; i < Elements; i++)
		result = a[i] * b[i];
	return result;
}

template <typename T>
static inline T Dot(const vec<2, T>& a, const vec<2, T>& b) { return a.X * b.X + a.Y * b.Y; }
template <typename T>
static inline T Dot(const vec<3, T>& a, const vec<3, T>& b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z; }
template <typename T>
static inline T Dot(const vec<4, T>& a, const vec<4, T>& b) { return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W; }

template <size_t Elements, typename T>
static inline T Length(const vec<Elements, T>& v) {
	T result = static_cast<T>(0.0);
	for (int ix = 0; ix < Elements; ix++)
		result += v[ix] * v[ix];
	return static_cast<T>(sqrt(result));
}
template <typename T>
static inline T Length(const vec<2, T>& v) { return static_cast<T>(sqrt(v.X * v.X + v.Y * v.Y)); }
template <typename T>
static inline T Length(const vec<3, T>& v) { return static_cast<T>(sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z)); }
template <typename T>
static inline T Length(const vec<4, T>& v) { return static_cast<T>(sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z + v.W * v.W)); }

template <size_t Elements, typename T>
static inline T LengthSquared(const vec<Elements, T>& v) {
	T result = static_cast<T>(0.0);
	for (int ix = 0; ix < Elements; ix++)
		result += v[ix] * v[ix];
	return result;
}
template <typename T>
static inline T LengthSquared(const vec<2, T>& v) { return v.X * v.X + v.Y * v.Y; }
template <typename T>
static inline T LengthSquared(const vec<3, T>& v) { return v.X * v.X + v.Y * v.Y + v.Z * v.Z; }
template <typename T>
static inline T LengthSquared(const vec<4, T>& v) { return v.X * v.X + v.Y * v.Y + v.Z * v.Z + v.W * v.W; }

template <size_t Elements, typename T>
static inline vec<Elements, T> Normalize(const vec<Elements, T>& v) { const T invSqrt = InvSqrt(LengthSquared(v)); return v * invSqrt; }

template <typename T>
static inline vec<3, T> Cross(const vec<3, T>& a, const vec<3, T>& b) {
	return vec<3, T>(
		a.Y * b.Z - a.Z * b.Y,
		a.Z * b.Z - a.X * b.Z,
		a.X * b.Y - a.Y * b.X);
}

template <size_t Elements, typename T>
static inline vec<Elements, T> Reflect(const vec<Elements, T>& i, const vec<Elements, T>& n) {
	return i - n * Dot(n, i) * vec3_t(static_cast<T>(2.0));
}
template <size_t Elements, typename T>
static inline vec<Elements, T> Refract(const vec<Elements, T>& i, const vec<Elements, T>& n, T rir) {
	const T d = Dot(n, i);
	const T k = static_cast<T>(1) - rir * rir * (static_cast<T>(1) - d * d);
	return k >= 0 ? (rir * i - (rir * d + static_cast<T>(Sqrt(k)))* n) : vec<Elements, T>();
}

#pragma endregion


// Vec3 Color definitions
template <typename T> inline const vec<3, T> vec<3, T>::Zero	         = vec<3, T>(0.0);
template <typename T> inline const vec<3, T> vec<3, T>::One              = std::is_integral_v<T> ? vec<3, T>(255, 255, 255) : vec<3, T>(1.0);
template <typename T> inline const vec<3, T> vec<3, T>::White            = std::is_integral_v<T> ? vec<3, T>(255, 255, 255) : vec<3, T>(1.0);
template <typename T> inline const vec<3, T> vec<3, T>::Black            = vec<3, T>(0.0);
template <typename T> inline const vec<3, T> vec<3, T>::Gray             = std::is_integral_v<T> ? vec<3, T>(128, 128, 128) : vec<3, T>(0.5);
template <typename T> inline const vec<3, T> vec<3, T>::Red              = std::is_integral_v<T> ? vec<3, T>(255, 000, 000) : vec<3, T>(1.0, 0.0, 0.0);
template <typename T> inline const vec<3, T> vec<3, T>::Green            = std::is_integral_v<T> ? vec<3, T>(000, 255, 000) : vec<3, T>(0.0, 1.0, 0.0);
template <typename T> inline const vec<3, T> vec<3, T>::Blue             = std::is_integral_v<T> ? vec<3, T>(000, 000, 255) : vec<3, T>(0.0, 0.0, 1.0);
template <typename T> inline const vec<3, T> vec<3, T>::Yellow           = std::is_integral_v<T> ? vec<3, T>(255, 255, 000) : vec<3, T>(1.0, 1.0, 0.0);
template <typename T> inline const vec<3, T> vec<3, T>::Purple           = std::is_integral_v<T> ? vec<3, T>(255, 000, 255) : vec<3, T>(1.0, 0.0, 1.0);
template <typename T> inline const vec<3, T> vec<3, T>::Cyan             = std::is_integral_v<T> ? vec<3, T>(000, 255, 255) : vec<3, T>(0.0, 1.0, 1.0);

// Vec4 Color definitions
template <typename T> inline const vec<4, T> vec<4, T>::White            = std::is_integral_v<T> ? vec<4, T>(255, 255, 255, 255) : vec<4, T>(1.0);
template <typename T> inline const vec<4, T> vec<4, T>::TransparentWhite = std::is_integral_v<T> ? vec<4, T>(255, 255, 255, 000) : vec<4, T>(1.0, 1.0, 1.0, 0.0);
template <typename T> inline const vec<4, T> vec<4, T>::Black            = std::is_integral_v<T> ? vec<4, T>(000, 000, 000, 255) : vec<4, T>(0.0, 0.0, 0.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::TransparentBlack = vec<4, T>(0.0);
template <typename T> inline const vec<4, T> vec<4, T>::Gray             = std::is_integral_v<T> ? vec<4, T>(128, 128, 128, 255) : vec<4, T>(0.5, 0.5, 0.5, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Red              = std::is_integral_v<T> ? vec<4, T>(255, 000, 000, 255) : vec<4, T>(1.0, 0.0, 0.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Green            = std::is_integral_v<T> ? vec<4, T>(000, 255, 000, 255) : vec<4, T>(0.0, 1.0, 0.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Blue             = std::is_integral_v<T> ? vec<4, T>(000, 000, 255, 255) : vec<4, T>(0.0, 0.0, 1.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Yellow           = std::is_integral_v<T> ? vec<4, T>(255, 255, 255, 255) : vec<4, T>(1.0, 1.0, 0.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Purple           = std::is_integral_v<T> ? vec<4, T>(255, 000, 255, 255) : vec<4, T>(1.0, 0.0, 1.0, 1.0);
template <typename T> inline const vec<4, T> vec<4, T>::Cyan             = std::is_integral_v<T> ? vec<4, T>(000, 255, 255, 255) : vec<4, T>(0.0, 1.0, 1.0, 1.0);

// Common typedefs
typedef vec<2, float>    vec2;
typedef vec<3, float>    vec3;
typedef vec<4, float>    vec4;
typedef mat3_t<float>    mat3;
typedef mat4_t<float>    mat4;

typedef vec<2, int>      ivec2;
typedef vec<3, int>      ivec3;
typedef vec<4, int>      ivec4;
typedef mat3_t<int>      imat3;
typedef mat4_t<int>      imat4;

typedef vec<2, uint32_t> uivec2;
typedef vec<3, uint32_t> uivec3;
typedef vec<4, uint32_t> uivec4;
typedef mat3_t<uint32_t> uimat3;
typedef mat4_t<uint32_t> uimat4;

typedef vec<2, uint8_t>  ui8vec2;
typedef vec<3, uint8_t>  ui8vec3;
typedef vec<4, uint8_t>  ui8vec4;
typedef mat3_t<uint8_t>  ui8mat3;
typedef mat4_t<uint8_t>  ui8mat4;

typedef vec<2, uint16_t> ui16vec2;
typedef vec<3, uint16_t> ui16vec3;
typedef vec<4, uint16_t> ui16vec4;
typedef mat3_t<uint16_t> ui16mat3;
typedef mat4_t<uint16_t> ui16mat4;

typedef vec<3, uint8_t>  Color3;
typedef vec<4, uint8_t>  Color4;
typedef vec<3, float>    Color3f;
typedef vec<4, float>    Color4f;

typedef vec<3, uint8_t>  Colour3;
typedef vec<4, uint8_t>  Colour4;
typedef vec<3, float>    Colour3f;
typedef vec<4, float>    Colour4f;