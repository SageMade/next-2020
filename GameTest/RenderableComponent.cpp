#include "stdafx.h"
#include "RenderableComponent.h"

RenderableComponent* RenderableComponent::SetMesh(const LineMesh::Sptr& mesh) { _mesh = mesh; return this; }

void RenderableComponent::Render() {
	LineRenderer3D::RenderLineMesh(_mesh);
}
