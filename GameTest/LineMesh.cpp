#include "stdafx.h"
#include "LineMesh.h"

LineMesh::LineMesh() : _lines(std::vector<Line>()){ }

void LineMesh::AddLine(const Line& line) {
	_lines.push_back(line);
}

void LineMesh::AddLine(const vec3& start, const vec3& end, const vec3& color) {
	_lines.push_back({ start, end, color });
}

void LineMesh::AddLine(float sx, float sy, float ex, float ey, float r, float g, float b) {
	_lines.push_back({ vec3(sx, sy, 0.0f), vec3(ex, ey, 0.0f), vec3(r, g, b) });
}

void LineMesh::AddLine(float sx, float sy, float sz, float ex, float ey, float ez, float r, float g, float b) {
	_lines.push_back({ vec3(sx, sy, sz), vec3(ex, ey, ez), vec3(r, g, b) });
}

LineMesh& LineMesh::operator+=(const Line& line) {
	AddLine(line);
	return *this;
}
